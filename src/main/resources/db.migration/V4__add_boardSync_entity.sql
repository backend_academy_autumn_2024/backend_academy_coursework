CREATE TABLE "boardSync" (
	id          uuid PRIMARY KEY,
	notion_database_id VARCHAR(256),
	jetbrains_board_id    VARCHAR(256),
	user_id uuid,
	CONSTRAINT user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id)

)