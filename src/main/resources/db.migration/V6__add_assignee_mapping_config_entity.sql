CREATE TABLE "assigneeMappingConfig" (
	id          uuid PRIMARY KEY,
	notion_member_id VARCHAR(256),
	jetbrains_user_id    VARCHAR(256),
	board_sync uuid,
	CONSTRAINT board_sync_fkey FOREIGN KEY (board_sync) REFERENCES "boardSync"(id)
)