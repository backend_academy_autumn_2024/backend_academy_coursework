--ALTER TABLE "user"
--DROP COLUMN 'jspaceusername';

ALTER TABLE "user"
  ADD COLUMN jspace_username VARCHAR(256);

UPDATE "user"
    SET jspace_username = 'yupichkin'
WHERE id = '4f9bc63a-a150-11ee-8c90-0242ac120002';