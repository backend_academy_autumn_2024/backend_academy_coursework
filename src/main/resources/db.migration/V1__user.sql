CREATE TABLE "user" (
	id          uuid PRIMARY KEY,
	notion_bearer_token VARCHAR(256),
	jetbrains_permanent_token    VARCHAR(256)
)