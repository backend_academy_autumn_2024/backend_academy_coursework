CREATE TABLE "ticketSync" (
	id          uuid PRIMARY KEY,
    hashcode integer,
	notion_page_id VARCHAR(256),
	jetbrains_issue_id    VARCHAR(256),
	board_sync uuid,
	CONSTRAINT board_sync_fkey FOREIGN KEY (board_sync) REFERENCES "boardSync"(id)
)