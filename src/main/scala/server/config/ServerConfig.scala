package server.config

case class PostgresConfig(url: String, user: String, password: String, poolSize: Int)

case class HttpServer(port: Int)

case class ServerConfig(database: PostgresConfig, http: HttpServer)
