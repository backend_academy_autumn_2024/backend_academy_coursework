package server.domain.ticket

import server.common.tethys.TethysInstances
import server.domain.board.CreateSyncConnection
import sttp.tapir.Schema
import tethys.derivation.semiauto.{jsonReader, jsonWriter}
import tethys.{JsonReader, JsonWriter}

import java.util.UUID

case class TicketResponse(
    hashcode: Int,
    notionPageId: String,
    jetbrainsIssueId: String,
    boardSync: UUID,
)

object TicketResponse extends TethysInstances {
  implicit val ticketResponseReader: JsonReader[TicketResponse] = jsonReader

  implicit val ticketResponseWriter: JsonWriter[TicketResponse] = jsonWriter

  implicit val ticketResponseSchema: Schema[TicketResponse] = Schema.derived
    .description(
      "Запрос создания и синхронизации доски из Jetbrains Space c id = jetbrainsBoardId с доской из Notion с id = notionDatabaseId",
    )
  /*
        .in(query[String]("projectId").description("ID проекта в Jetbrains Space ОТКУДА перенести доску"))
      .in(query[String]("boardId").description("ID доски в Jetbrains Space которую нужно перенести"))
      .in(query[String]("notionPageId").description("ID страницы в Notion КУДА перенести доску"))
   */
}
