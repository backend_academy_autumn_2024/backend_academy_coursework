package server.domain.ticket

import java.util.UUID

case class CreateTicket(
    hashcode: Int,
    notionPageId: String,
    jetbrainsIssueId: String,
    boardSync: UUID,
)
