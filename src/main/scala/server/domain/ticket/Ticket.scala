package server.domain.ticket

import server.domain.board.{BoardConnectionResponse, CreateBoardConnection}

import java.util.UUID

final case class Ticket(
    id: UUID,
    hashcode: Int,
    notionPageId: String,
    jetbrainsIssueId: String,
    boardSync: UUID,
) {
  def toResponse: TicketResponse = TicketResponse(
    hashcode = hashcode,
    notionPageId = notionPageId,
    jetbrainsIssueId = jetbrainsIssueId,
    boardSync = boardSync,
  )
}

object Ticket {
  def fromCreateTicket(id: UUID, createTicket: CreateTicket): Ticket =
    Ticket(
      id = id,
      hashcode = createTicket.hashcode,
      notionPageId = createTicket.notionPageId,
      jetbrainsIssueId = createTicket.jetbrainsIssueId,
      boardSync = createTicket.boardSync,
    )
}
