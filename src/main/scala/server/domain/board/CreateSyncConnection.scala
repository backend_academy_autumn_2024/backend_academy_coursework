package server.domain.board

import server.common.tethys.TethysInstances
import sttp.tapir.Schema
import sttp.tapir.Schema.annotations.description
import tethys.derivation.semiauto.{jsonReader, jsonWriter}
import tethys.{JsonReader, JsonWriter}

import java.util.UUID

final case class CreateSyncConnection(
    @description("ID доски в Jetbrains Space ОТКУДА перенести доску")
    jetbrainsBoardId: String,
    @description("ID страницы в Notion КУДА перенести доску")
    notionParentPageId: String,
    @description("Уникальный идентификатор пользователя")
    userId: UUID,
)

object CreateSyncConnection extends TethysInstances {
  implicit val boardConnectionResponseReader: JsonReader[CreateSyncConnection] = jsonReader

  implicit val boardConnectionResponseWriter: JsonWriter[CreateSyncConnection] = jsonWriter

  implicit val boardConnectionResponseSchema: Schema[CreateSyncConnection] = Schema.derived
    .description(
      "Запрос создания и синхронизации доски из Jetbrains Space c id = jetbrainsBoardId с доской из Notion с id = notionDatabaseId",
    )
  /*
        .in(query[String]("projectId").description("ID проекта в Jetbrains Space ОТКУДА перенести доску"))
      .in(query[String]("boardId").description("ID доски в Jetbrains Space которую нужно перенести"))
      .in(query[String]("notionPageId").description("ID страницы в Notion КУДА перенести доску"))
   */
}
