package server.domain.board

import server.common.tethys.TethysInstances
import server.domain.user.UserResponse
import sttp.tapir.Schema
import tethys.derivation.semiauto.{jsonReader, jsonWriter}
import tethys.{JsonReader, JsonWriter}

import java.util.UUID

final case class BoardConnectionResponse(
    id: UUID,
    notionDatabaseId: String,
    jetbrainsBoardId: String,
    userId: UUID,
)

object BoardConnectionResponse extends TethysInstances {
  implicit val boardConnectionResponseReader: JsonReader[BoardConnectionResponse] = jsonReader

  implicit val boardConnectionResponseWriter: JsonWriter[BoardConnectionResponse] = jsonWriter

  implicit val boardConnectionResponseSchema: Schema[BoardConnectionResponse] = Schema.derived
    .description(
      "Соединение доски из Jetbrains Space c id = jetbrainsBoardId с доской из Notion с id = notionDatabaseId",
    )
}
