package server.domain.board

import server.common.tethys.TethysInstances
import sttp.tapir.Schema
import sttp.tapir.Schema.annotations.description
import sttp.tapir.generic.Derived
import tethys.derivation.semiauto.{jsonReader, jsonWriter}
import tethys.{JsonReader, JsonWriter}

import java.util.UUID
final case class CreateBoardConnection(
    @description("ID доски в Jetbrains Space ОТКУДА перенести доску")
    jetbrainsBoardId: String,
    @description("ID страницы в Notion КУДА перенести доску")
    notionDatabaseId: String,
    @description("Уникальный идентификатор пользователя")
    userId: UUID,
)

object CreateBoardConnection {

  /*
        .in(query[String]("projectId").description("ID проекта в Jetbrains Space ОТКУДА перенести доску"))
      .in(query[String]("boardId").description("ID доски в Jetbrains Space которую нужно перенести"))
      .in(query[String]("notionPageId").description("ID страницы в Notion КУДА перенести доску"))
   */
}
