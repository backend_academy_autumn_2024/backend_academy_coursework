package server.domain.board

import server.common.tethys.TethysInstances
import server.domain.user.{CreateUser, User, UserResponse}
import sttp.tapir.Schema
import tethys.derivation.semiauto.{jsonReader, jsonWriter}
import tethys.{JsonReader, JsonWriter}

import java.util.UUID

final case class BoardConnection(
    id: UUID,
    notionDatabaseId: String,
    jetbrainsBoardId: String,
    userId: UUID,
    //TODO: maybe add something like "last time synchronized?"
) {
  def toResponse: BoardConnectionResponse =
    BoardConnectionResponse(
      id = id,
      notionDatabaseId = notionDatabaseId,
      jetbrainsBoardId = jetbrainsBoardId,
      userId = userId,
    )
}

object BoardConnection {
  def fromCreateBoardConnection(
      id: UUID,
      createBoardConnection: CreateBoardConnection,
  ): BoardConnection =
    BoardConnection(
      id = id,
      notionDatabaseId = createBoardConnection.notionDatabaseId,
      jetbrainsBoardId = createBoardConnection.jetbrainsBoardId,
      userId = createBoardConnection.userId,
    )
}
