package server.domain.assignee

import server.domain.board.{BoardConnection, BoardConnectionResponse}

import java.util.UUID

final case class AssigneeMappingConfig(
    id: UUID,
    notionMemberId: String,
    jetbrainsUserId: String,
    boardSync: UUID,
) {
  def toResponse: AssigneeMappingConfigResponse =
    AssigneeMappingConfigResponse(
      notionMemberId = notionMemberId,
      jetbrainsUserId = jetbrainsUserId,
    )
}

object AssigneeMappingConfig {
  def fromCreateAssigneeMappingConfig(
      id: UUID,
      boardId: UUID,
      createAssigneeMappingConfig: CreateAssigneeMappingConfig,
  ): AssigneeMappingConfig =
    AssigneeMappingConfig(
      id = id,
      notionMemberId = createAssigneeMappingConfig.notionMemberId,
      jetbrainsUserId = createAssigneeMappingConfig.jebrainsUserId,
      boardSync = boardId,
    )
}
