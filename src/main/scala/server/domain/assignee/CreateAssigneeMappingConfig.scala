package server.domain.assignee

import server.common.tethys.TethysInstances
import sttp.tapir.Schema
import tethys.derivation.semiauto.{jsonReader, jsonWriter}
import tethys.{JsonReader, JsonWriter}

case class CreateAssigneeMappingConfig(
    jebrainsUserId: String,
    notionMemberId: String,
)

object CreateAssigneeMappingConfig extends TethysInstances {
  implicit val createAssigneeConnectionReader: JsonReader[CreateAssigneeMappingConfig] = jsonReader

  implicit val createAssigneeConnectionWriter: JsonWriter[CreateAssigneeMappingConfig] = jsonWriter

  implicit val createAssigneeConnectionSchema: Schema[CreateAssigneeMappingConfig] = Schema.derived
    .description(
      "Настройки маппинга пользователей досок в качестве полей assignee",
    )

}
