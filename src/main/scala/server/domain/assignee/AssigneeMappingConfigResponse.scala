package server.domain.assignee

import server.common.tethys.TethysInstances
import server.domain.board.BoardConnectionResponse
import sttp.tapir.Schema
import tethys.derivation.semiauto.{jsonReader, jsonWriter}
import tethys.{JsonReader, JsonWriter}

import java.util.UUID

case class AssigneeMappingConfigResponse(
    notionMemberId: String,
    jetbrainsUserId: String,
)

object AssigneeMappingConfigResponse extends TethysInstances {
  implicit val assigneeMappingConfigResponseReader: JsonReader[AssigneeMappingConfigResponse] =
    jsonReader

  implicit val assigneeMappingConfigResponseWriter: JsonWriter[AssigneeMappingConfigResponse] =
    jsonWriter

  implicit val assigneeMappingConfigResponseSchema: Schema[AssigneeMappingConfigResponse] =
    Schema.derived
      .description(
        "Возврат настройки маппинга пользователей из довок в Jetbrains Space и Notion",
      )

}
