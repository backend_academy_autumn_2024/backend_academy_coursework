package server.domain.assignee

import server.common.tethys.TethysInstances
import server.domain.board.BoardConnectionResponse
import sttp.tapir.Schema
import sttp.tapir.generic.Derived
import tethys.derivation.semiauto.{jsonReader, jsonWriter}
import tethys.{JsonReader, JsonWriter}

case class AssigneesResponse(
    jSpaceAssignees: List[JSpaceAssigneeResponse],
    notionAssignees: List[NotionAssigneeResponse],
)

case class JSpaceAssigneeResponse(id: String, username: String)

case class NotionAssigneeResponse(id: String, name: String)

object AssigneesResponse extends TethysInstances {

  implicit val notionAssigneeResponseReader: JsonReader[NotionAssigneeResponse] = jsonReader

  implicit val notionAssigneeResponseWriter: JsonWriter[NotionAssigneeResponse] = jsonWriter

  implicit val jSpaceAssigneeResponseReader: JsonReader[JSpaceAssigneeResponse] = jsonReader

  implicit val jSpaceAssigneeResponseWriter: JsonWriter[JSpaceAssigneeResponse] = jsonWriter

  implicit val assigneeResponseReader: JsonReader[AssigneesResponse] = jsonReader

  implicit val assigneeResponseWriter: JsonWriter[AssigneesResponse] = jsonWriter

  import sttp.tapir.generic.auto._
  import sttp.tapir.generic.Derived

  implicit val assigneeResponseSchema: Schema[AssigneesResponse] =
    implicitly[Derived[Schema[AssigneesResponse]]].value
      .modify(_.jSpaceAssignees)(_.description("Пользователи из доски Jetbrains Space"))
      .modify(_.notionAssignees)(_.description("Пользователи из Teamspace Notion"))

//
//  implicit val assigneeResponseSchema1: Schema[NotionAssignee] = Schema.derived
//    .description("fdsafdsa ")
//  implicit val assigneeResponseSchema2: Schema[JSpaceAssignee] = Schema.derived
//    .description("fdsafdsafsa ")
}
