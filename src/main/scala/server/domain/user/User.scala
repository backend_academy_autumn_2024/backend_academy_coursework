package server.domain.user

import java.time.Instant
import java.util.UUID

final case class User(
    id: UUID,
    notionBearerToken: String,
    jspaceUsername: String,
    jetbrainsPermanentToken: String,
) {
  def toResponse: UserResponse =
    UserResponse(
      id = id,
      notionBearerToken = notionBearerToken,
      jspaceUsername: String,
      jetbrainsPermanentToken = jetbrainsPermanentToken,
    )
}

object User {
  def fromCreateUser(id: UUID, jspaceUsername: String, createUser: CreateUser): User =
    User(
      id = id,
      jspaceUsername = jspaceUsername,
      notionBearerToken = createUser.notionBearerToken,
      jetbrainsPermanentToken = createUser.jetbrainsPermanentToken,
    )
}
