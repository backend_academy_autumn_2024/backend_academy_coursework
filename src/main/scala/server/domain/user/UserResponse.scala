package server.domain.user

import server.common.tethys.TethysInstances
import sttp.tapir.Schema
import tethys.derivation.semiauto.{jsonReader, jsonWriter}
import tethys.{JsonReader, JsonWriter}

import java.util.UUID

final case class UserResponse(
    id: UUID,
    notionBearerToken: String,
    jspaceUsername: String,
    jetbrainsPermanentToken: String,
)

object UserResponse extends TethysInstances {
  implicit val orderResponseReader: JsonReader[UserResponse] = jsonReader

  implicit val orderResponseWriter: JsonWriter[UserResponse] = jsonWriter

  implicit val orderResponseSchema: Schema[UserResponse] = Schema.derived
    .description("Пользователь нашего сервиса маппинга из Jetbrains Space в Notion")
}
