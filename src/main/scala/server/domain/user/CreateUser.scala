package server.domain.user

import server.common.tethys.TethysInstances
import sttp.tapir.Schema
import tethys.derivation.semiauto.{jsonReader, jsonWriter}
import tethys.{JsonReader, JsonWriter}

case class CreateUser(
    linkToJetbrainsSpace: String,
    notionBearerToken: String,
    jetbrainsPermanentToken: String,
)

object CreateUser extends TethysInstances {
  implicit val createOrderReader: JsonReader[CreateUser] = jsonReader

  implicit val createOrderWriter: JsonWriter[CreateUser] = jsonWriter

  implicit val createOrderSchema: Schema[CreateUser] = Schema.derived
    .description("Запрос регистрации пользователя в сервисе")
}
