package server.controller

import cats.Applicative
import sttp.tapir._
import sttp.tapir.server.ServerEndpoint
import _root_.server.common.controller.Controller

class ExampleController[F[_]: Applicative] extends Controller[F] {
  val hello: ServerEndpoint[Any, F] =
    endpoint.get
      .withTag("Example")
      .summary("Сказать привет")
      .in("api" / "v1" / "hello" / query[String]("name"))
      .out(stringBody)
      .serverLogicSuccess(name => Applicative[F].pure(s"Hello, $name!"))

  override val endpoints: List[ServerEndpoint[Any, F]] =
    List(
      //listOrders,
      //getOrder,
      //deleteOrder
    )
}

object ExampleController {
  def make[F[_]: Applicative]: ExampleController[F] = new ExampleController[F]
}
