package server.controller

import cats.{FlatMap, Functor, Monad}
import cats.implicits.{toFlatMapOps, toFunctorOps, toTraverseOps}
import client.jb_space.model.response.board.BoardResponse
import server.common.controller.Controller
import server.domain.board.BoardConnectionResponse
import server.domain.board.CreateSyncConnection
import server.domain.ticket.TicketResponse
import server.domain.assignee.CreateAssigneeMappingConfig
import server.domain.assignee.AssigneeMappingConfigResponse
import server.domain.assignee.AssigneesResponse
import server.service.{BoardService, MapperService, UserService}
import sttp.model.StatusCode
import sttp.tapir.json.tethysjson.jsonBody
import sttp.tapir.server.ServerEndpoint
import sttp.tapir.{endpoint, query, statusCode}
import sttp.tapir._

import java.util.UUID

class MapperController[F[_]: Monad](
    mapperService: MapperService[F],
    boardService: BoardService[F],
    userService: UserService[F],
) extends Controller[F] {
  //TODO: deal with correct "https" encryption working (for now server works only for default "http")
//  val getBoards: ServerEndpoint[Any, F] =
//    endpoint.get
//      .summary("Получить список досок пользователя")
//      .in("api" / "v1" / "jspace" / path[UUID]("userId"))
//      .out(jsonBody[Option[List[BoardResponse]]])
//      .serverLogicSuccess(jSpaceService.getBoardsForUser)

  val mapBoardFromProjectIntoPageInNotion: ServerEndpoint[Any, F] =
    endpoint.post
      .summary("Создать доску из Jetbrains Space в page Notion")
      .in("api" / "v1" / "jspace")
      .in(jsonBody[CreateSyncConnection])
      .out(statusCode)
      .out(jsonBody[BoardConnectionResponse])
      .errorOut(statusCode)
      .errorOut(jsonBody[String])
      .serverLogic { input =>
        import cats.implicits.toFlatMapOps
        for {
          user <- userService.get(input.userId)
          boardMapResult <- user
            .toRight("User was not found")
            .traverse(validUser =>
              mapperService
                .mapBoardToNotion(validUser, input.jetbrainsBoardId, input.notionParentPageId),
            )
          serverOutput = boardMapResult.flatten.fold(
            str => Left((StatusCode.NotFound, str)),
            boardConnection => Right((StatusCode.Accepted, boardConnection)),
          )
        } yield serverOutput
      }

  val mapIssuesFromJSpaceToNotion: ServerEndpoint[Any, F] =
    endpoint.patch
      .summary("Синхронизировать доски из Jetbrains Space в Notion")
      .in("api" / "v1" / "jspace" / "map")
      .in(query[UUID]("UserUUID"))
      .in(query[UUID]("BoardConnectionUUID"))
      .out(statusCode)
      .out(jsonBody[String])
      .errorOut(statusCode)
      .errorOut(jsonBody[String])
      .serverLogic { input =>
        import cats.implicits.toFlatMapOps

        for {
          user <- userService.get(input._1)
          board <- boardService.get(input._2)

          result <- (user, board) match {
            case (Some(user), Some(board)) =>
              if (user.id != board.userId)
                Monad[F].pure(
                  Left(s"Board with UUID=${input._2} for user with UUID=${input._1} was not found"),
                )
              else {
                mapperService.mapTicketsToNotion(user, board).map(Right(_))

              }
            case (None, _) => Monad[F].pure(Left(s"User with UUID=${input._1} was not found"))
            case (_, None) => Monad[F].pure(Left(s"Board with UUID=${input._2} was not found"))
          }

          serverOutput = result.fold(
            str => Left((StatusCode.NotFound, str)),
            result => Right((StatusCode.Accepted, result.toString())),
          )
        } yield serverOutput
      }

  val getUsersOfBoard: ServerEndpoint[Any, F] =
    endpoint.get
      .summary("Получить пользователей из досок")
      .in("api" / "v1" / "board" / "assignees")
      .in(query[UUID]("UserUUID"))
      .in(query[UUID]("BoardConnectionUUID"))
      .out(statusCode)
      .out(jsonBody[AssigneesResponse])
      .errorOut(statusCode)
      .errorOut(jsonBody[String])
      .serverLogic { input =>
        import cats.implicits.toFlatMapOps

        for {
          //TODO: change services exit values to Either from Option and service should deal error messages
          user <- userService.get(input._1)
          board <- boardService.get(input._2)

          result <- (user, board) match {
            case (Some(user), Some(board)) =>
              if (user.id != board.userId)
                Monad[F].pure(
                  Left(s"Board with UUID=${input._2} for user with UUID=${input._1} was not found"),
                )
              else {
                mapperService.getAssigneesOfBoards(user, board)
              }
            case (None, _) => Monad[F].pure(Left(s"User with UUID=${input._1} was not found"))
            case (_, None) => Monad[F].pure(Left(s"Board with UUID=${input._2} was not found"))
          }

          serverOutput = result.fold(
            str => Left((StatusCode.NotFound, str)),
            result => Right((StatusCode.Accepted, result)),
          )
        } yield serverOutput
      }

  val setAssigneesConfigForBoardConnection: ServerEndpoint[Any, F] =
    endpoint.put
      .summary("Получить пользователей из досок")
      .in("api" / "v1" / "board" / "assignees")
      .in(query[UUID]("UserUUID"))
      .in(query[UUID]("BoardConnectionUUID"))
      .in(jsonBody[List[CreateAssigneeMappingConfig]])
      .out(statusCode)
      .out(jsonBody[List[AssigneeMappingConfigResponse]])
      .errorOut(statusCode)
      .errorOut(jsonBody[String])
      .serverLogic { input =>
        import cats.implicits.toFlatMapOps

        for {
          user <- userService.get(input._1)
          board <- boardService.get(input._2)

          result <- (user, board) match {
            case (Some(user), Some(board)) =>
              if (user.id != board.userId)
                Monad[F].pure(
                  Left(s"Board with UUID=${input._2} for user with UUID=${input._1} was not found"),
                )
              else {
                mapperService.setAssigneesConfigForBoard(user, board, input._3)
              }
            case (None, _) => Monad[F].pure(Left(s"User with UUID=${input._1} was not found"))
            case (_, None) => Monad[F].pure(Left(s"Board with UUID=${input._2} was not found"))
          }

          serverOutput = result.fold(
            str => Left((StatusCode.NotFound, str)),
            result => Right((StatusCode.Accepted, result)),
          )
        } yield serverOutput
      }

  //  val listOrders: ServerEndpoint[Any, F] =
  //    endpoint.get
  //      .summary("Список заказов")
  //      .in("api" / "v1" / "order")
  //      .out(jsonBody[List[OrderResponse]])
  //      .serverLogicSuccess(_ => orderService.list)
  //
  //  val getOrder: ServerEndpoint[Any, F] =
  //    endpoint.get
  //      .summary("Получить заказ")
  //      .in("api" / "v1" / "order" / path[UUID]("orderId"))
  //      .out(jsonBody[Option[OrderResponse]])
  //      .serverLogicSuccess(orderService.get)
  //
  //  val deleteOrder: ServerEndpoint[Any, F] =
  //    endpoint.delete
  //      .summary("Удалить заказов")
  //      .in("api" / "v1" / "order" / path[UUID]("orderId"))
  //      .out(jsonBody[Option[OrderResponse]])
  //      .serverLogicSuccess(orderService.delete)

  override val endpoints: List[ServerEndpoint[Any, F]] =
    List(
      mapBoardFromProjectIntoPageInNotion,
      mapIssuesFromJSpaceToNotion,
      getUsersOfBoard,
      setAssigneesConfigForBoardConnection,
      //listOrders,
      //getOrder,
      //deleteOrder
    )
      .map(_.withTag("Mapper Service"))
}

object MapperController {
  def make[F[_]: Monad](
      mapperService: MapperService[F],
      boardService: BoardService[F],
      userService: UserService[F],
  ): MapperController[F] =
    new MapperController[F](mapperService, boardService, userService)
}
