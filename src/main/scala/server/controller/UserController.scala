package server.controller

import server.common.controller.Controller
import server.service.UserService
import sttp.tapir.endpoint
import sttp.tapir.server.ServerEndpoint
import sttp.tapir._
import sttp.tapir.json.tethysjson.jsonBody
import _root_.server.domain.user.CreateUser
import _root_.server.domain.user.UserResponse
import cats.{Applicative, FlatMap, Monad}
import sttp.model.StatusCode

class UserController[F[_]: Monad](userService: UserService[F]) extends Controller[F] {

  val createUser: ServerEndpoint[Any, F] =
    endpoint.post
      .summary("Создать пользователя")
      .in("api" / "v1" / "user")
      .in(jsonBody[CreateUser])
      .out(statusCode)
      .out(jsonBody[UserResponse])
      .errorOut(stringBody)
      //.errorOut(statusCode)
      .serverLogic { createUser =>
        import cats.implicits.toFunctorOps
        import cats.implicits.toFlatMapOps
        import cats.implicits.toTraverseOps

        for {
          isLinkToJSpaceValid <- userService.validateLinkToJSpace(createUser.linkToJetbrainsSpace)
          isValidJSpaceToken <- isLinkToJSpaceValid.traverse(username =>
            userService.validateJSToken(createUser.jetbrainsPermanentToken, username),
          )
          isValidNotionToken <- userService.validateNotionToken(createUser.notionBearerToken)

          userCreatingResult = for {
            jSpaceUsername <- isLinkToJSpaceValid
            _ <- isValidJSpaceToken
            _ <- isValidNotionToken
            localResult = userService
              .create(createUser, jSpaceUsername)

          } yield localResult

          finalResult <- userCreatingResult match {
            case Right(userResponseInF) =>
              userResponseInF.map(response => Right(StatusCode.Accepted, response))
            case Left(errorMessage) => Applicative[F].pure(Left(errorMessage))
          }
          //.map(response => Right(StatusCode.Accepted, response)
//          result <- (isValidJSpaceToken, isValidNotionToken) match {
//            case (Right(_), Right(_)) =>
//              userService
//                .create(createUser, "fdsa")
//                .map(response => Right(StatusCode.Accepted, response))
//            case (Left(error1), Left(error2)) => Applicative[F].pure(Left(s"$error1, $error2"))
//            case (_, Left(error2))            => Applicative[F].pure(Left(error2))
//            case (Left(error1), _)            => Applicative[F].pure(Left(error1))
//          }

        } yield finalResult
      }

  override val endpoints: List[ServerEndpoint[Any, F]] =
    List(
      createUser,
    )
      .map(_.withTag("User"))
}

object UserController {
  def make[F[_]: Monad](userService: UserService[F]): UserController[F] =
    new UserController[F](userService)
}
