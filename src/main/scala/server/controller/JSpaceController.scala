package server.controller

import server.common.controller.Controller
import server.service.UserService
import server.service.JSpaceService
import sttp.tapir.endpoint
import sttp.tapir.server.ServerEndpoint
import sttp.tapir._
import sttp.tapir.json.tethysjson.jsonBody
import _root_.server.domain.user.CreateUser
import _root_.server.domain.user.UserResponse
import cats.Functor
import cats.implicits.toFunctorOps
import client.commons.dto.BoardMetadata
import client.jb_space.model.response.board.BoardResponse
import sttp.model.StatusCode

import java.util.UUID

class JSpaceController[F[_]: Functor](jSpaceService: JSpaceService[F]) extends Controller[F] {
  //TODO: deal with correct "https" encryption working (for now server works only for default "http")

  val getBoards: ServerEndpoint[Any, F] =
    endpoint.get
      .summary("Получить список досок пользователя")
      .in("api" / "v1" / "jspace" / path[UUID]("userId"))
      .out(jsonBody[Option[List[BoardResponse]]])
      .serverLogicSuccess(jSpaceService.getBoardsForUser)

  //TODO: change input request body to
  // case class BoardParams(
  // userId: UUID,
  // projectId: String,
  // boardId: String,
  // notionPageId: String
  // and with .in(jsonBody[BoardParams])
  val mapBoardFromProjectIntoPageInNotion: ServerEndpoint[Any, F] =
    endpoint.post
      .summary("Создать доску из Jetbrains Space в page Notion")
      .in("api" / "v1" / "jspace")
      .in(query[UUID]("userId").description("ID пользователя"))
      .in(
        query[String]("projectId").description(
          "ID проекта в Jetbrains Space ОТКУДА перенести доску",
        ),
      )
      .in(
        query[String]("boardId").description("ID доски в Jetbrains Space которую нужно перенести"),
      )
      .in(query[String]("notionPageId").description("ID страницы в Notion КУДА перенести доску"))
      .out(statusCode)
      .out(jsonBody[String])
      .serverLogic { input =>
        jSpaceService
          .mapBoardToNotion(input._1, input._2, input._3, input._4)
          .map {
            case Right(value) => Right(StatusCode.Accepted, value)
            case Left(value)  => Right(StatusCode.NotFound, value)
          }
      }

  //  val listOrders: ServerEndpoint[Any, F] =
  //    endpoint.get
  //      .summary("Список заказов")
  //      .in("api" / "v1" / "order")
  //      .out(jsonBody[List[OrderResponse]])
  //      .serverLogicSuccess(_ => orderService.list)
  //
  //  val getOrder: ServerEndpoint[Any, F] =
  //    endpoint.get
  //      .summary("Получить заказ")
  //      .in("api" / "v1" / "order" / path[UUID]("orderId"))
  //      .out(jsonBody[Option[OrderResponse]])
  //      .serverLogicSuccess(orderService.get)
  //
  //  val deleteOrder: ServerEndpoint[Any, F] =
  //    endpoint.delete
  //      .summary("Удалить заказов")
  //      .in("api" / "v1" / "order" / path[UUID]("orderId"))
  //      .out(jsonBody[Option[OrderResponse]])
  //      .serverLogicSuccess(orderService.delete)

  override val endpoints: List[ServerEndpoint[Any, F]] =
    List(
      getBoards,
      mapBoardFromProjectIntoPageInNotion,
      //listOrders,
      //getOrder,
      //deleteOrder
    )
      .map(_.withTag("Jetbrains Space"))
}

object JSpaceController {
  def make[F[_]: Functor](jSpaceService: JSpaceService[F]): JSpaceController[F] =
    new JSpaceController[F](jSpaceService)
}
