package server

import cats.MonadThrow
import cats.data.OptionT
import cats.effect.std.Env
import cats.effect.{ExitCode, IO, IOApp}
import client.jb_space.{HttpJBSpaceClient, JBSpaceClient}
import client.jb_space.model.configuration.JBSpaceClientConfiguration
import client.notion.{HttpNotionClient, NotionClient}
import client.notion.model.configuration.NotionClientConfiguration
import com.comcast.ip4s.{Host, Port}
import com.typesafe.config.{Config, ConfigFactory}
import doobie.Transactor
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.server.Router
import pureconfig.ConfigSource
import server.config.ServerConfig
import server.controller.{ExampleController, JSpaceController, MapperController, UserController}
import server.repository.{
  AssigneeMappingRepository,
  BoardRepository,
  TicketRepository,
  UserRepository,
}
import server.service.{BoardService, JSpaceService, MapperService, NotionService, UserService}
import server.repository.postgresql.{
  AssigneeMappingRepositoryPostgresql,
  BoardRepositoryPostgresql,
  TicketRepositoryPostgresql,
  UserRepositoryPostgresql,
}
import sttp.client3.asynchttpclient.cats.AsyncHttpClientCatsBackend
import sttp.tapir.server.http4s.Http4sServerInterpreter
import sttp.tapir.swagger.bundle.SwaggerInterpreter
import pureconfig.generic.auto._
import server.database.FlywayMigration
import server.database.transactor.makeTransactor

import java.util.UUID

object Main extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {

    val dbConfig = ConfigSource.default.loadOrThrow[ServerConfig]
    val clientConfig: Config = ConfigFactory.load()
    val jBSpaceClientConfiguration: JBSpaceClientConfiguration =
      JBSpaceClientConfiguration.load(clientConfig)
    val notionClientConfiguration: NotionClientConfiguration =
      NotionClientConfiguration.load(clientConfig)

    makeTransactor[IO](dbConfig.database).use { implicit xa: Transactor[IO] =>
      val userRepo: UserRepository[IO] = new UserRepositoryPostgresql[IO]
      val boardRepo: BoardRepository[IO] = new BoardRepositoryPostgresql[IO]
      val ticketRepo: TicketRepository[IO] = new TicketRepositoryPostgresql[IO]
      val assigneesMappingRepo: AssigneeMappingRepository[IO] =
        new AssigneeMappingRepositoryPostgresql[IO]

      println(UUID.randomUUID())

      for {
        _ <- FlywayMigration.migrate[IO](dbConfig.database)

        sttpBackend <- AsyncHttpClientCatsBackend[IO]()
        notionClient: NotionClient[IO] = new HttpNotionClient[IO](
          sttpBackend,
          notionClientConfiguration,
        )
        jBSpaceClient: JBSpaceClient[IO] = new HttpJBSpaceClient[IO](
          sttpBackend,
          jBSpaceClientConfiguration,
        )

        endpoints <- IO.delay {
          val userService: UserService[IO] =
            UserService.make[IO](userRepo, jBSpaceClient, notionClient)
          val jSpaceService: JSpaceService[IO] =
            JSpaceService.make[IO](userRepo, jBSpaceClient, notionClient)
          val boardService: BoardService[IO] =
            BoardService.make[IO](boardRepo, ticketRepo, assigneesMappingRepo)
          val notionService: NotionService[IO] = NotionService.make[IO](notionClient)
          val mapperService: MapperService[IO] =
            MapperService.make[IO](jSpaceService, boardService, notionService)
          List(
            ExampleController.make[IO],
            UserController.make[IO](userService),
            //JSpaceController.make[IO](JSpaceService.make[IO](userRepo, jBSpaceClient, notionClient)),
            MapperController.make[IO](mapperService, boardService, userService),
          ).flatMap(_.endpoints)
        }
        swagger = SwaggerInterpreter()
          .fromServerEndpoints[IO](endpoints, "jetbrains space to notion board mapper", "1.0.0")
        routes = Http4sServerInterpreter[IO]()
          .toRoutes(swagger ++ endpoints)
        port <- getPort[IO]
        _ <- EmberServerBuilder
          .default[IO]
          .withHost(Host.fromString("localhost").get) //TODO: add localhost as config property
          .withPort(port)
          .withHttpApp(Router("/" -> routes).orNotFound)
          .build
          .use { server =>
            for {
              _ <- IO.println(
                s"Go to http://localhost:${server.address.getPort}/docs to open SwaggerUI. Press ENTER key to exit.",
              )
              _ <- IO.readLine
            } yield ()
          }
      } yield ExitCode.Success
    }
  }

  private def getPort[F[_]: Env: MonadThrow]: F[Port] =
    OptionT(Env[F].get("HTTP_PORT"))
      .toRight("HTTP_PORT not found")
      .subflatMap(ps =>
        ps.toIntOption.toRight(s"Expected int in HTTP_PORT env variable, but got $ps"),
      )
      .subflatMap(pi => Port.fromInt(pi).toRight(s"No such port $pi"))
      .leftMap(new IllegalArgumentException(_))
      .rethrowT

}
