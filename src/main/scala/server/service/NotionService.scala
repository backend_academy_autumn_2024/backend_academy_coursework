package server.service

import cats.implicits.{toFunctorOps, toTraverseOps}
import cats.{Applicative, FlatMap, Monad}
import client.commons.dto.BoardMetadata
import client.jb_space.JBSpaceClient
import client.jb_space.model.response.board.BoardResponse
import client.notion.NotionClient
import client.notion.model.request.createDatabase.CreateDatabaseInPageRequest
import client.notion.model.request.createDatabase.models.PageParent
import client.notion.model.request.createDatabase.models.propertiesOfDb.PropertiesOfDb
import client.notion.model.request.createDatabase.models.propertiesOfDb.models.selectConfig.{
  SelectOption,
  StatusConfig,
  TagsConfig,
}
import client.notion.model.request.createDatabase.models.title.Title
import client.notion.model.request.createDatabase.models.title.models.Text
import client.notion.model.request.createIssue.{
  CreateIssueInDatabaseRequest,
  NotionPage,
  NotionPageWithMetadata,
}
import client.notion.model.request.updatePage.UpdatePageInDatabaseRequest
import client.notion.model.response.NotionPageResponse
import client.notion.model.response.members.NotionMembers
import server.domain.user.User
import server.repository.UserRepository
import server.service.JSpaceService.Impl

import java.util.UUID

trait NotionService[F[_]] {
  def getAssigneesOfBoard(
      notionBearerToken: String,
      notionDatabaseId: String,
  ): F[Either[String, NotionMembers]]

  def createDatabaseInPage(
      notionBearerToken: String,
      createDatabaseRequest: CreateDatabaseInPageRequest,
  ): F[String]

  def createPageInDatabase(
      notionBearerToken: String,
      databaseId: String,
      notionPage: NotionPage,
  ): F[NotionPageResponse]
  def updatePageInDatabase(
      notionBearerToken: String,
      databaseId: String,
      notionPage: NotionPage,
      notionPageId: String,
  ): F[NotionPageResponse]

  def getCreateDatabaseRequest(
      notionPageId: String,
      boardMetadata: BoardMetadata,
  ): CreateDatabaseInPageRequest

  //def mapBoardToNotion(input: (UUID, String, String, String)): F[Option[String]]
}

object NotionService {
  private class Impl[F[_]: Monad](notionClient: NotionClient[F]) extends NotionService[F] {

    def getCreateDatabaseRequest(
        notionPageId: String,
        boardMetadata: BoardMetadata,
    ): CreateDatabaseInPageRequest = {
      val pageParent: PageParent = PageParent(
        page_id = notionPageId,
      )
      val titleOfDb: List[Title] = List(
        Title(
          text = Text(
            content = boardMetadata.boardName,
            link = None, //TODO: maybe add link to jspace board?
          ),
        ),
      )
      val tags: TagsConfig = TagsConfig(
        options = boardMetadata.tagsOptions.map(SelectOption(_)),
      )
      val statuses = StatusConfig(
        options = boardMetadata.statusOptions.map(SelectOption(_)),
      )
      val propertiesOfDb: PropertiesOfDb = PropertiesOfDb(
        PropertiesOfDb.defaultPropertiesOfDb :++
          List(tags, statuses),
      )
      CreateDatabaseInPageRequest(
        parent = pageParent,
        title = titleOfDb,
        properties = propertiesOfDb,
      )
    }

    override def createDatabaseInPage(
        notionBearerToken: String,
        createDatabaseRequest: CreateDatabaseInPageRequest,
    ): F[String] =
      notionClient
        .createDatabaseInPage(notionBearerToken, createDatabaseRequest)
        .map(_.id)

    override def createPageInDatabase(
        notionBearerToken: String,
        databaseId: String,
        notionPage: NotionPage,
    ): F[NotionPageResponse] =
      notionClient.createPageInDatabase(
        notionBearerToken,
        CreateIssueInDatabaseRequest(
          databaseId = databaseId,
          notionPage = notionPage,
        ),
      )

    def updatePageInDatabase(
        notionBearerToken: String,
        databaseId: String,
        notionPage: NotionPage,
        notionPageId: String,
    ): F[NotionPageResponse] =
      notionClient.updatePageInDatabase(
        notionBearerToken,
        UpdatePageInDatabaseRequest(notionPageId, notionPage),
      )
    //      for {
//        _ <- notionClient.updatePageInDatabase(notionBearerToken, notionPages.head.)
//      } yield

    override def getAssigneesOfBoard(
        notionBearerToken: String,
        notionDatabaseId: String,
    ): F[Either[String, NotionMembers]] =
      for {
        members <- notionClient.getMembersOfNotionTeamSpace(notionBearerToken)
      } yield Right(members)
  }
  def make[F[_]: Monad](notionClient: NotionClient[F]): NotionService[F] =
    new Impl[F](notionClient)
}
