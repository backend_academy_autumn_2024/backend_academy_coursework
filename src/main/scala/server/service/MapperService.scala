package server.service


import cats.{Applicative, Monad}
import client.jb_space.JBSpaceClient
import client.jb_space.model.response.issue.Issue
import client.notion.NotionClient
import client.notion.model.request.createDatabase.CreateDatabaseInPageRequest
import client.notion.model.request.createIssue.{CreateIssueInDatabaseRequest, NotionPage}
import client.notion.model.request.properties.NotionAssignee
import client.notion.model.response.NotionPageResponse
import server.domain.assignee.{
  AssigneeMappingConfig,
  AssigneeMappingConfigResponse,
  AssigneesResponse,
  CreateAssigneeMappingConfig,
  JSpaceAssigneeResponse,
  NotionAssigneeResponse,
}
import server.domain.board.{BoardConnection, BoardConnectionResponse, CreateBoardConnection}
import server.domain.ticket.{CreateTicket, Ticket, TicketResponse}
import server.domain.user.User
import server.repository.UserRepository

import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.traverse._
import java.util.UUID
import scala.collection.immutable.List

trait MapperService[F[_]] {
  def setAssigneesConfigForBoard(
      user: User,
      board: BoardConnection,
      createAssigneeConnection: List[CreateAssigneeMappingConfig],
  ): F[Either[String, List[AssigneeMappingConfigResponse]]]

  def getAssigneesOfBoards(user: User, board: BoardConnection): F[Either[String, AssigneesResponse]]

  def mapBoardToNotion(
      user: User,
      boardId: String,
      notionPageId: String,
  ): F[Either[String, BoardConnectionResponse]]

  def mapTicketsToNotion(user: User, boardConnection: BoardConnection): F[List[TicketResponse]]

}

object MapperService {
  private class Impl[F[_]: Monad](
      boardService: BoardService[F],
      jSpaceService: JSpaceService[F],
      notionService: NotionService[F],
  ) extends MapperService[F] {

    def mapJSpaceIssueToNotionPage(
        issue: Issue,
        assigneeMappingConfigs: List[AssigneeMappingConfig],
    ): NotionPage = {
      //TODO: change this subfunc to implicit class of Issue class
      val optionNotionAssignee: Option[NotionAssignee] = issue.assignee.flatMap { jSpaceAssignee =>
        assigneeMappingConfigs.collectFirst {
          case notionAssignee if notionAssignee.jetbrainsUserId == jSpaceAssignee.id =>
            NotionAssignee(notionAssignee.notionMemberId)
        }
      }
      NotionPage.createFromJSpaceIssue(issue, optionNotionAssignee)
    }

    override def mapBoardToNotion(
        user: User,
        boardId: String,
        notionPageId: String,
    ): F[Either[String, BoardConnectionResponse]] = {
      //TODO: add some validation of existence of page board and project...



      for {
        projectId <- jSpaceService.getProjectIdOfBoard(user, boardId)
        //TODO: is this abusing of traverse usage for getting right value from either - is ok?
        boardMetadata <- projectId.traverse(jSpaceService.getBoardMetadata(_, boardId, user))
        //boardMetadata <- jSpaceService.getBoardMetadata(projectId, boardId, user)
        createDatabaseRequest = boardMetadata.map(
          notionService.getCreateDatabaseRequest(notionPageId, _),
        )
        //_ = println(CreateDatabaseInPageRequest.createDatabaseInPageRequestEncoder.apply(createDatabaseRequest))
        createdNotionDatabaseId <- createDatabaseRequest.traverse(
          notionService.createDatabaseInPage(user.notionBearerToken, _),
        )

        createBoardConnection <- createdNotionDatabaseId
          .map(id =>
            CreateBoardConnection(
              notionDatabaseId = id,
              jetbrainsBoardId = boardId,
              userId = user.id,
            ),
          )
          .traverse(boardService.create)

        //result <- createBoardConnection.traverse(boardService.create)

      } yield createBoardConnection
    }

    override def mapTicketsToNotion(
        user: User,
        boardConnection: BoardConnection,
    ): F[List[TicketResponse]] = {
      for {
        issuesToMapFromJSpace <- jSpaceService.getAllIssuesOnBoard(
          user,
          boardConnection.jetbrainsBoardId,
        )
        existedTicketsFromDb <- boardService.getTicketsForBoard(boardConnection)

        issuesAndTheirTickets = issuesToMapFromJSpace.map(issue =>
          (issue, existedTicketsFromDb.find(_.jetbrainsIssueId == issue.id)),
        )

        oldIssuesAndTicket = issuesAndTheirTickets.collect { case (issue, Some(ticket)) =>
          (issue, ticket)
        }

        newIssues = issuesAndTheirTickets.collect { case (issue, None) =>
          issue
        }

        assigneesMappingConfig <- boardService.getAssigneeMappingConfigForBoard(boardConnection)

        oldMappedIssuesAndNotionId = oldIssuesAndTicket.map(tuple =>
          (mapJSpaceIssueToNotionPage(tuple._1, assigneesMappingConfig), tuple._2),
        )
        oldTicketsResponses <- processOldTicketsFromJSpace(
          oldMappedIssuesAndNotionId,
          user,
          boardConnection,
        )

        newTickets <- newIssues.traverse { issue =>
          notionService
            .createPageInDatabase(
              user.notionBearerToken,
              boardConnection.notionDatabaseId,
              mapJSpaceIssueToNotionPage(issue, assigneesMappingConfig),
            )
            .map { pageAsResponse =>
              println(pageAsResponse.assignee)
              CreateTicket(
                hashcode = pageAsResponse.toPage().hashCode(),
                notionPageId = pageAsResponse.id,
                jetbrainsIssueId = issue.id,
                boardSync = boardConnection.id,
              )
            }
        }
        newTicketResponses <- newTickets.traverse { createTicket =>
          boardService.saveTicketForBoard(createTicket)
        }

//        newPagesCreateResults <- notionService.createPageInDatabase(user.notionBearerToken, boardConnection.notionDatabaseId, newMappedIssues)
//        firstPageResponse = newPagesCreateResults.head
//        createTicket = CreateTicket(
//          hashcode = firstPageResponse.toPage().hashCode(),
//          notionPageId = firstPage.,
//          jetbrainsIssueId = ???,
//          boardSync = ???
//        )
        //oldPagesUpdateResults <- notionService.updatePagesInDatabase(user.notionBearerToken, boardConnection.notionDatabaseId, oldMappedIssuesAndNotionId)

      } yield oldTicketsResponses ::: newTicketResponses
    }

    def processOldTicketsFromJSpace(
        oldIssues: List[(NotionPage, Ticket)],
        user: User,
        boardConnection: BoardConnection,
    ): F[List[TicketResponse]] = {

      for {
        updatedNotionPagesAndTickets <- oldIssues
          .filter(notionPageAndTicket =>
            notionPageAndTicket._1.hashCode() != notionPageAndTicket._2.hashcode,
          )
          .traverse { notionPageAndTicket =>
            val beforePage = notionPageAndTicket._1
            val result: F[NotionPageResponse] = notionService.updatePageInDatabase(
              user.notionBearerToken,
              boardConnection.notionDatabaseId,
              notionPageAndTicket._1,
              notionPageAndTicket._2.notionPageId,
            )

            result.map((_, notionPageAndTicket._2, beforePage))
          }
        result <- updatedNotionPagesAndTickets.traverse { input =>
          val beforePage = input._3
          val notionPage = input._1.toPage()
          //TODO: there is a bug - hashcode has been changed after getting response from api due to description stored as None or in empty list
          val updatedTicket = input._2.copy(hashcode = notionPage.hashCode())
          boardService.updateTicketForBoard(updatedTicket)
        }
      } yield result
    }

    override def getAssigneesOfBoards(
        user: User,
        board: BoardConnection,
    ): F[Either[String, AssigneesResponse]] = {
      for {
        jSpaceAssignees <- jSpaceService.getAssigneesOfBoard(
          user,
          board.jetbrainsBoardId,
        )
        notionAssignees <- notionService.getAssigneesOfBoard(
          user.notionBearerToken,
          board.notionDatabaseId,
        )
        assigneeResponse = for {
          validJSpaceAssignees <- jSpaceAssignees
          validNotionAssignees <- notionAssignees
          jSpaceAssigneeResponses = validJSpaceAssignees.data.map { assignee =>
            JSpaceAssigneeResponse(assignee.id, assignee.username)
          }
          notionAssigneesResponses = validNotionAssignees.members.map { assignee =>
            NotionAssigneeResponse(assignee.id, assignee.name)
          }
        } yield AssigneesResponse(jSpaceAssigneeResponses, notionAssigneesResponses)
      } yield assigneeResponse
    }

    override def setAssigneesConfigForBoard(
        user: User,
        board: BoardConnection,
        createAssigneeConnection: List[CreateAssigneeMappingConfig],
    ): F[Either[String, List[AssigneeMappingConfigResponse]]] = {

      for {
        boardsAssignees <- getAssigneesOfBoards(user, board)
        result1 = for {
          validBoardAssignees <- boardsAssignees
          isAllAssigneesContainsForJSpace = createAssigneeConnection.map(_.jebrainsUserId).forall {
            jetbrainsUserId =>
              validBoardAssignees.jSpaceAssignees.map(_.id).contains(jetbrainsUserId)
          }
          isAllAssigneesContainsForNotion = createAssigneeConnection.map(_.notionMemberId).forall {
            notionMemberId =>
              validBoardAssignees.notionAssignees.map(_.id).contains(notionMemberId)
          }
          isAllInputUsersIdExists =
            isAllAssigneesContainsForJSpace & isAllAssigneesContainsForNotion
          result =
            if (isAllInputUsersIdExists) {
              val result321: F[Either[String, List[AssigneeMappingConfigResponse]]] = boardService
                .updateAssigneeMappingConfigForBoard(board, createAssigneeConnection)
                .map(Right(_))
              result321
            } else {
              val result3214: F[Either[String, List[AssigneeMappingConfigResponse]]] =
                Monad[F].pure(Left("invalid users Ids"))
              result3214
            }
        } yield result
        //TODO: fix this st*pid sh*t due to boardsAssignees returned as either
        fdsaf <- result1.getOrElse(
          Monad[F].pure(Right(List(AssigneeMappingConfigResponse("fdsafas", "fdsajk;fsa")))),
        )

      } yield fdsaf
    }
  }

  def make[F[_]: Monad](
      jSpaceService: JSpaceService[F],
      boardService: BoardService[F],
      notionService: NotionService[F],
  ): MapperService[F] =
    new Impl[F](boardService, jSpaceService, notionService)

}
