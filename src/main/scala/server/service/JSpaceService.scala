package server.service

import cats.{Applicative, FlatMap, Monad}
import client.commons.dto.BoardMetadata
import client.jb_space.model.response.board.BoardResponse
import server.repository.UserRepository
import client.jb_space.JBSpaceClient
import client.jb_space.model.response.issue.Issue
import client.jb_space.model.response.issue.tag.TagsData
import client.jb_space.model.response.project.{ProjectMembers, ProjectsData}
import client.notion.NotionClient
import client.notion.model.request.createDatabase.models.propertiesOfDb.PropertiesOfDb
import client.notion.model.request.createDatabase.models.title.Title
import client.notion.model.request.createDatabase.models.title.models.Text
import client.notion.model.request.createDatabase.models.PageParent
import client.notion.model.request.createDatabase.models.propertiesOfDb.models.selectConfig.{
  SelectOption,
  StatusConfig,
  TagsConfig,
}
import client.notion.model.request.createDatabase.{CreateDatabaseInPageRequest, models}
import client.notion.model.request.createIssue.{CreateIssueInDatabaseRequest, NotionPage}
import io.circe.generic.semiauto.deriveEncoder
import server.domain.user.User

import java.util.UUID

trait JSpaceService[F[_]] {
  def getAssigneesOfBoard(
      user: User,
      jetbrainsBoardId: String,
  ): F[Either[String, ProjectMembers]]

  def getProjectIdOfBoard(
      user: User,
      boardId: String,
  ): F[Either[String, String]]

  def getAllIssuesOnBoard(user: User, boardId: String): F[List[Issue]]

  def getBoardsForUser(userId: UUID): F[Option[List[BoardResponse]]]

  def mapBoardToNotion(
      userId: UUID,
      projectId: String,
      boardId: String,
      notionPageId: String,
  ): F[Either[String, String]]

  def getBoardMetadata(projectId: String, boardId: String, user: User): F[BoardMetadata]
}

object JSpaceService {
  private class Impl[F[_]: Monad](
      userRepository: UserRepository[F],
      jBSpaceClient: JBSpaceClient[F],
      notionClient: NotionClient[F],
  ) extends JSpaceService[F] {

    override def getBoardsForUser(userId: UUID): F[Option[List[BoardResponse]]] = {
      import cats.implicits.toTraverseOps
      import cats.FlatMap.ops.toAllFlatMapOps
      for {
        userOption <- userRepository.get(userId)
        result <- userOption
          .map(user =>
            jBSpaceClient
              .getAllProjects(user.jetbrainsPermanentToken, user.jspaceUsername)
              .map(_.data)
              .map(_.flatMap(_.boards)),
          )
          .sequence
      } yield result
    }

    override def mapBoardToNotion(
        userId: UUID,
        projectId: String,
        boardId: String,
        notionPageId: String,
    ): F[Either[String, String]] = {
      //TODO: add some validation of existence of page board and project...
      import cats.FlatMap.ops.toAllFlatMapOps
      import cats.implicits.toTraverseOps
      //import cats.syntax.flatMap._
      for {
        userAsOption <- userRepository.get(userId)
        fdsa = println(userAsOption)
        //TODO: change to option map somehow
        result <- userAsOption match {
          case Some(user) =>
            for {
              createDatabaseRequest <- getCreateDatabaseRequest(
                projectId,
                boardId,
                notionPageId,
                user,
              )
              _ = println(
                CreateDatabaseInPageRequest.createDatabaseInPageRequestEncoder.apply(
                  createDatabaseRequest,
                ),
              )
              idOfCreatedDatabase <- notionClient.createDatabaseInPage(
                user.notionBearerToken,
                createDatabaseRequest,
              )
//              issuesToMapFromJSpace <- jBSpaceClient.getAllIssuesOnBoard(
//                user.jetbrainsPermanentToken,
//                boardId,
//              )
//              _ = issuesToMapFromJSpace.data.map(_.hashCode()).map(println(_))
//              mappedIssues = issuesToMapFromJSpace.data.map(issue =>
//                NotionPage.createFromJSpaceIssue(issue),
//              )
//
//              responseOfCreatingPagesInNotion <- mappedIssues.map { notionPage =>
//                val createIssueInDatabaseRequest = CreateIssueInDatabaseRequest(
//                  databaseId = idOfCreatedDatabase.id,
//                  notionPage = notionPage,
//                )
//                notionClient.createPageInDatabase(
//                  user.notionBearerToken,
//                  createIssueInDatabaseRequest,
//                )
//              }.sequence

              //mappedIssues = mapIssuesToNotionPages(issuesToMapFromJSpace)
              _ = println(idOfCreatedDatabase)
              _ = println(idOfCreatedDatabase)
              _ = println(idOfCreatedDatabase)
              _ = println(idOfCreatedDatabase)
              _ = println(idOfCreatedDatabase)
              _ = println(idOfCreatedDatabase)
              _ = println(idOfCreatedDatabase)
              _ = println(idOfCreatedDatabase)

            } yield Right("something") //yield (resultOfCreatingDatabase)
          case None =>
            val res: F[Either[String, String]] = Applicative[F].pure(Left("User not found"))
            res
        }
      } yield result
    }

    def getCreateDatabaseRequest(
        projectId: String,
        boardId: String,
        notionPageId: String,
        user: User,
    ): F[CreateDatabaseInPageRequest] = {
      import cats.FlatMap.ops.toAllFlatMapOps

      val pageParent: PageParent = PageParent(
        page_id = notionPageId,
      )
      //TODO: remake CreateDatabaseInPageRequest generation to object of class building etc: new CreateDatabaseInPageRequest( boardName = ..., tags = ..., ...)
      for {
        boardName <- jBSpaceClient.getNameOfBoard(
          user.jetbrainsPermanentToken,
          user.jspaceUsername,
          boardId,
        )

        tagsOptions <- getTagsSelectOptions(projectId, user)
        tags = TagsConfig(
          options = tagsOptions,
        )

        statusOptions <- getStatusSelectOptions(projectId, user)
        statuses = StatusConfig(
          options = statusOptions,
        )

        titleOfDb: List[Title] = List(
          Title(
            text = Text(
              content = boardName.name,
              link = None, //TODO: maybe add link to jspace board?
            ),
          ),
        )
        //name: NameConfig = NameConfig(()),
        //                          description: DescriptionContent = DescriptionContent(()),
        //                          //priority: PriorityContent,
        //                          dueDate: DueDateConfig = DueDateConfig(()),
        //                          tags: TagsConfig
        propertiesOfDb: PropertiesOfDb = PropertiesOfDb(
          PropertiesOfDb.defaultPropertiesOfDb :++
            List(tags, statuses),
        )
        createDatabaseInPageRequest: CreateDatabaseInPageRequest = CreateDatabaseInPageRequest(
          parent = pageParent,
          title = titleOfDb,
          properties = propertiesOfDb,
        )
      } yield createDatabaseInPageRequest
    }

    override def getBoardMetadata(
        projectId: String,
        boardId: String,
        user: User,
    ): F[BoardMetadata] = {
      import cats.implicits.toFunctorOps
      import cats.implicits.toFlatMapOps

      for {
        boardName <- jBSpaceClient.getNameOfBoard(
          user.jetbrainsPermanentToken,
          user.jspaceUsername,
          boardId,
        )
        tagsOptions <- jBSpaceClient
          .getAllTagsOnProject(user.jetbrainsPermanentToken, user.jspaceUsername, projectId)
          .map(_.data.map(tag => tag.name))
        statusOptions <- jBSpaceClient
          .getAllStatusesOnProject(user.jetbrainsPermanentToken, user.jspaceUsername, projectId)
          .map(list => list.map(status => status.name))
      } yield BoardMetadata(
        boardName = boardName.name,
        tagsOptions = tagsOptions,
        statusOptions = statusOptions,
      )
    }

    def getTagsSelectOptions(projectId: String, user: User): F[List[SelectOption]] = {
      import cats.FlatMap.ops.toAllFlatMapOps
      jBSpaceClient
        .getAllTagsOnProject(user.jetbrainsPermanentToken, user.jspaceUsername, projectId)
        .map(_.data.map(tag => SelectOption(tag.name)))
    }

    def getStatusSelectOptions(projectId: String, user: User): F[List[SelectOption]] = {

      import cats.FlatMap.ops.toAllFlatMapOps
      jBSpaceClient
        .getAllStatusesOnProject(user.jetbrainsPermanentToken, user.jspaceUsername, projectId)
        .map(list => list.map(status => SelectOption(status.name)))
    }

    override def getAllIssuesOnBoard(
        user: User,
        boardId: String,
    ): F[List[Issue]] = {
      import cats.implicits.toFunctorOps
      jBSpaceClient
        .getAllIssuesOnBoard(user.jetbrainsPermanentToken, user.jspaceUsername, boardId)
        .map(_.data)
    }

    override def getProjectIdOfBoard(
        user: User,
        boardId: String,
    ): F[Either[String, String]] = {
      import cats.implicits.toFunctorOps
      for {
        allProjects <- jBSpaceClient.getAllProjects(
          user.jetbrainsPermanentToken,
          user.jspaceUsername,
        )
        result = allProjects.data
          .find(project => project.boards.map(_.id).contains(boardId))
          .map(_.id) //TODO: change to collect
          .toRight("Board wad not found")
      } yield result
    }

    override def getAssigneesOfBoard(
        user: User,
        jetbrainsBoardId: String,
    ): F[Either[String, ProjectMembers]] = {
      import cats.FlatMap.ops.toAllFlatMapOps
      import cats.implicits.toTraverseOps
      for {
        projectId <- getProjectIdOfBoard(user, jetbrainsBoardId)
        members <- projectId.traverse(
          jBSpaceClient.getAllMembersOfProject(user.jetbrainsPermanentToken, user.jspaceUsername, _),
        )
      } yield members
    }
  }

  def make[F[_]: Monad](
      userRepository: UserRepository[F],
      jBSpaceClient: JBSpaceClient[F],
      notionClient: NotionClient[F],
  ): JSpaceService[F] =
    new Impl[F](userRepository, jBSpaceClient, notionClient)
}
