package server.service

import cats.effect.std.UUIDGen
import server.domain.user.{CreateUser, User, UserResponse}
import server.repository.UserRepository
import cats.{FlatMap, Monad}
import cats.syntax.functor._
import cats.syntax.flatMap._
import client.jb_space.JBSpaceClient
import client.jb_space.model.response.auth.{AuthError, AuthorizedRightResponse}
import client.notion.NotionClient

import java.util.UUID

trait UserService[F[_]] {
  def validateLinkToJSpace(linkToJetbrainsSpace: String): F[Either[String, String]]

  def validateNotionToken(notionBearerToken: String): F[Either[String, String]]

  def validateJSToken(jetbrainsPermanentToken: String, username: String): F[Either[String, String]]

  //def create(createOrder: CreateOrder): F[OrderResponse]

  //def list: F[List[OrderResponse]]

  //def get(id: UUID): F[Option[OrderResponse]]

  //def delete(id: UUID): F[Option[OrderResponse]]

  def create(createUser: CreateUser, jspaceUsername: String): F[UserResponse]
  def get(id: UUID): F[Option[User]]
}

object UserService {
  private class Impl[F[_]: UUIDGen: Monad](
      userRepository: UserRepository[F],
      jBSpaceClient: JBSpaceClient[F],
      notionClient: NotionClient[F],
  ) extends UserService[F] {
    def create(createUser: CreateUser, jspaceUsername: String): F[UserResponse] =
      for {
        id <- UUIDGen[F].randomUUID
        //now <- Clock[F].realTimeInstant
        user = User.fromCreateUser(id, jspaceUsername, createUser)
        _ <- userRepository.create(user)
      } yield user.toResponse
//
//    override def list: F[List[OrderResponse]] =
//      orderRepository.list
//        .map(_.map(_.toResponse))
//
//    override def get(id: UUID): F[Option[OrderResponse]] =
//      orderRepository
//        .get(id)
//        .map(_.map(_.toResponse))
//
//    override def delete(id: UUID): F[Option[OrderResponse]] =
//      orderRepository
//        .delete(id)
//        .map(_.map(_.toResponse))

    override def validateJSToken(
        jetbrainsPermanentToken: String,
        username: String,
    ): F[Either[String, String]] =
      for {
        allRights <- jBSpaceClient.getAuthorizedRightsOfToken(jetbrainsPermanentToken, username)
        grantedRights = allRights.map(
          _.collect(authorizedRight =>
            authorizedRight match {
              case AuthorizedRightResponse(name, "GRANTED") => name
            },
          ),
        )
        requiredRights = jBSpaceClient.getRequiredRightOfToken

        isAllRightsContains: Either[String, String] = grantedRights match { //TODO: replace to fold
          case Right(currentRights) =>
            val missingRights: List[String] = currentRights.filterNot(requiredRights.contains(_))
            val result: Either[String, String] =
              if (missingRights.isEmpty) {
                Right("Token is correct")
              } else
                Left(
                  s"Your jetbrains space token doesn't have this required rights: ${missingRights.mkString(", ")}",
                )
            result
          case Left(authError) => Left(authError.error_description)
        }
      } yield isAllRightsContains

    //TODO: remove all token validations to their services (aka jspaceService and notionService)
    override def validateNotionToken(notionBearerToken: String): F[Either[String, String]] =
      Monad[F].pure(Right("token is valid"))

    override def get(id: UUID): F[Option[User]] = userRepository.get(id)

    override def validateLinkToJSpace(linkToJetbrainsSpace: String): F[Either[String, String]] =
      Monad[F].pure(jBSpaceClient.isApiLinkValidated(linkToJetbrainsSpace: String))
  }

  def make[F[_]: UUIDGen: Monad](
      userRepository: UserRepository[F],
      jBSpaceClient: JBSpaceClient[F],
      notionClient: NotionClient[F],
  ): UserService[F] =
    new Impl[F](userRepository, jBSpaceClient, notionClient)
}
