package server.service

import cats.{Applicative, FlatMap, Monad}
import cats.effect.IO
import cats.effect.std.UUIDGen
import client.commons.dto.BoardMetadata
import client.jb_space.JBSpaceClient
import client.jb_space.model.response.auth.AuthorizedRightResponse
import client.jb_space.model.response.board.BoardResponse
import client.jb_space.model.response.issue.Issue
import client.notion.NotionClient
import server.domain.board.{BoardConnection, BoardConnectionResponse, CreateBoardConnection}
import server.domain.user.{CreateUser, User, UserResponse}
import server.repository.{
  AssigneeMappingRepository,
  BoardRepository,
  TicketRepository,
  UserRepository,
}
import cats.implicits.{toFlatMapOps, toFunctorOps}
import server.domain.assignee.{
  AssigneeMappingConfig,
  AssigneeMappingConfigResponse,
  CreateAssigneeMappingConfig,
}
import server.domain.ticket.{CreateTicket, Ticket, TicketResponse}

import java.util.UUID

trait BoardService[F[_]] {

  def updateAssigneeMappingConfigForBoard(
      board: BoardConnection,
      createAssigneeConnection: List[CreateAssigneeMappingConfig],
  ): F[List[AssigneeMappingConfigResponse]]
  def getAssigneeMappingConfigForBoard(board: BoardConnection): F[List[AssigneeMappingConfig]]

  def create(createBoardConnection: CreateBoardConnection): F[BoardConnectionResponse]
  def get(id: UUID): F[Option[BoardConnection]]

  def saveTicketForBoard(createTicket: CreateTicket): F[TicketResponse]
  def getTicketsForBoard(boardConnection: BoardConnection): F[List[Ticket]]
  def updateTicketForBoard(updatedTicket: Ticket): F[TicketResponse]

  //def get(id: UUID)
}

object BoardService {
  private class Impl[F[_]: UUIDGen: Monad](
      boardRepository: BoardRepository[F],
      ticketRepository: TicketRepository[F],
      assigneeMappingRepository: AssigneeMappingRepository[F],
  ) extends BoardService[F] {
    def create(createBoardConnection: CreateBoardConnection): F[BoardConnectionResponse] =
      for {
        id <- UUIDGen[F].randomUUID
        //now <- Clock[F].realTimeInstant
        board = BoardConnection.fromCreateBoardConnection(id, createBoardConnection)
        _ <- boardRepository.create(board)
      } yield board.toResponse

    override def get(id: UUID): F[Option[BoardConnection]] =
      boardRepository.get(id)

    override def getTicketsForBoard(boardConnection: BoardConnection): F[List[Ticket]] =
      ticketRepository.getByBoardId(boardConnection.id)

    override def saveTicketForBoard(createTicket: CreateTicket): F[TicketResponse] =
      for {
        id <- UUIDGen[F].randomUUID
        //now <- Clock[F].realTimeInstant
        ticket = Ticket.fromCreateTicket(id, createTicket)
        _ <- ticketRepository.create(ticket)
      } yield ticket.toResponse

    override def updateAssigneeMappingConfigForBoard(
        board: BoardConnection,
        createAssigneeConnection: List[CreateAssigneeMappingConfig],
    ): F[List[AssigneeMappingConfigResponse]] = {
      import cats.implicits.toTraverseOps
      for {
        _ <- assigneeMappingRepository.deleteConfigForBoard(board.id)
        result <- createAssigneeConnection.traverse(createAssigneeMappingConfig =>
          createAssigneeMappingConfigInDb(board, createAssigneeMappingConfig),
        )
      } yield result
    }

    def createAssigneeMappingConfigInDb(
        board: BoardConnection,
        createAssigneeMappingConfig: CreateAssigneeMappingConfig,
    ): F[AssigneeMappingConfigResponse] =
      for {
        id <- UUIDGen[F].randomUUID
        assigneeMappingConfig = AssigneeMappingConfig.fromCreateAssigneeMappingConfig(
          id,
          board.id,
          createAssigneeMappingConfig,
        )
        _ <- assigneeMappingRepository.create(assigneeMappingConfig)
      } yield assigneeMappingConfig.toResponse

    override def getAssigneeMappingConfigForBoard(
        board: BoardConnection,
    ): F[List[AssigneeMappingConfig]] =
      for {
        assigneeMappingConfig <- assigneeMappingRepository.getByBoardId(board.id)
      } yield assigneeMappingConfig

    override def updateTicketForBoard(updatedTicket: Ticket): F[TicketResponse] =
      for {
        _ <- ticketRepository.update(updatedTicket)
      } yield (updatedTicket.toResponse)
  }

  def make[F[_]: UUIDGen: Monad](
      boardRepository: BoardRepository[F],
      ticketRepository: TicketRepository[F],
      assigneeMappingRepository: AssigneeMappingRepository[F],
  ): BoardService[F] =
    new Impl[F](boardRepository, ticketRepository, assigneeMappingRepository)

}
