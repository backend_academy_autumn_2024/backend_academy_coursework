package server.repository

import server.domain.board.BoardConnection
import server.domain.ticket.Ticket
import server.domain.user.User

import java.util.UUID

trait TicketRepository[F[_]] {
  def create(ticket: Ticket): F[Long]
  def update(ticket: Ticket): F[Long]
  def get(id: UUID): F[Option[Ticket]]
  def delete(id: UUID): F[Option[Ticket]]


  def getByBoardId(userId: UUID): F[List[Ticket]]

}
