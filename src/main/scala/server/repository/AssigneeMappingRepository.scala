package server.repository

import server.domain.assignee.AssigneeMappingConfig
import server.domain.ticket.Ticket

import java.util.UUID

trait AssigneeMappingRepository[F[_]] {
  def create(assigneeMappingConfig: AssigneeMappingConfig): F[Long]

  def get(id: UUID): F[Option[AssigneeMappingConfig]]

  def getByBoardId(boardId: UUID): F[List[AssigneeMappingConfig]]

  def deleteConfigForBoard(boardId: UUID): F[Long]
}
