package server.repository

import server.domain.user.User

import java.util.UUID

trait UserRepository[F[_]] {
  def create(user: User): F[Long]

  def get(id: UUID): F[Option[User]]

  def list: F[List[User]]

  def delete(id: UUID): F[Option[User]]
}
