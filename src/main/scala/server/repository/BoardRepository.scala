package server.repository

import server.domain.board.BoardConnection
import server.domain.user.User

import java.util.UUID

trait BoardRepository[F[_]] {
  def create(boardConnection: BoardConnection): F[Long]

  def get(id: UUID): F[Option[BoardConnection]]

  def getByUserId(userId: UUID): F[List[BoardConnection]]

  def delete(id: UUID): F[Option[BoardConnection]]
  def list: F[List[BoardConnection]]
  //
  //  def get(id: UUID): F[Option[Order]]
  //
  //  def delete(id: UUID): F[Option[Order]]

}
