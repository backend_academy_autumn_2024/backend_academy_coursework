package server.repository.postgresql

import cats.effect.kernel.MonadCancelThrow
import io.getquill.SnakeCase
import io.getquill.doobie.DoobieContext
import cats.implicits.toFunctorOps
import doobie.Transactor
import doobie.implicits._
import server.domain.user.User
import server.repository.UserRepository

import java.util.UUID

class UserRepositoryPostgresql[F[_]: MonadCancelThrow](implicit tr: Transactor[F])
    extends UserRepository[F] {

  private val ctx = new DoobieContext.Postgres(SnakeCase)
  import ctx._

  override def create(user: User): F[Long] = run {
    quote {
      querySchema[User]("\"user\"").insertValue(lift(user))
    }
  }.transact(tr)

  override def get(id: UUID): F[Option[User]] = run {
    quote {
      querySchema[User]("\"user\"").filter(_.id == lift(id))
    }
  }.transact(tr).map(_.headOption)

  override def list: F[List[User]] = run {
    quote(
      querySchema[User]("\"user\""),
    )
  }.transact(tr)

  override def delete(id: UUID): F[Option[User]] = run {
    quote {
      querySchema[User]("\"user\"")
        .filter(_.id == lift(id))
        .delete
        .returningMany(r => r)
    }
  }.transact(tr).map(_.headOption)

}
