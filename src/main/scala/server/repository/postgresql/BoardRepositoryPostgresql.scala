package server.repository.postgresql

import cats.effect.kernel.MonadCancelThrow
import io.getquill.SnakeCase
import io.getquill.doobie.DoobieContext
import cats.implicits.toFunctorOps
import doobie.Transactor
import doobie.implicits._
import server.domain.board.BoardConnection
import server.domain.user.User
import server.repository.BoardRepository

import java.util.UUID

class BoardRepositoryPostgresql[F[_]: MonadCancelThrow](implicit tr: Transactor[F])
    extends BoardRepository[F] {

  private val ctx = new DoobieContext.Postgres(SnakeCase)

  import ctx._

  override def create(boardConnection: BoardConnection): F[Long] = run {
    quote {
      querySchema[BoardConnection]("\"boardSync\"")
        .insertValue(lift(boardConnection))
    }
  }.transact(tr)

  override def get(id: UUID): F[Option[BoardConnection]] = run {
    quote {
      querySchema[BoardConnection]("\"boardSync\"").filter(_.id == lift(id))
    }
  }.transact(tr).map(_.headOption)

  override def getByUserId(userId: UUID): F[List[BoardConnection]] = run {
    quote {
      querySchema[BoardConnection]("\"boardSync\"").filter(_.userId == lift(userId))
    }
  }.transact(tr)

  override def delete(id: UUID): F[Option[BoardConnection]] = run {
    quote {
      querySchema[BoardConnection]("\"boardSync\"")
        .filter(_.id == lift(id))
        .delete
        .returningMany(r => r)
    }
  }.transact(tr).map(_.headOption)
  override def list: F[List[BoardConnection]] = run {
    quote(
      querySchema[BoardConnection]("\"boardSync\""),
    )
  }.transact(tr)
}
