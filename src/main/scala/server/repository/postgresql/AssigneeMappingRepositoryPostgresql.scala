package server.repository.postgresql

import cats.effect.kernel.MonadCancelThrow
import doobie.Transactor
import io.getquill.SnakeCase
import io.getquill.doobie.DoobieContext
import server.domain.board.BoardConnection
import cats.implicits.toFunctorOps
import doobie.implicits._
import server.domain.assignee.AssigneeMappingConfig
import server.repository.AssigneeMappingRepository

import java.util.UUID

class AssigneeMappingRepositoryPostgresql[F[_]: MonadCancelThrow](implicit tr: Transactor[F])
    extends AssigneeMappingRepository[F] {

  private val ctx = new DoobieContext.Postgres(SnakeCase)

  import ctx._

  override def create(assigneeMappingConfig: AssigneeMappingConfig): F[Long] = run {
    quote {
      querySchema[AssigneeMappingConfig]("\"assigneeMappingConfig\"")
        .insertValue(lift(assigneeMappingConfig))
    }
  }.transact(tr)

  override def get(id: UUID): F[Option[AssigneeMappingConfig]] = run {
    quote {
      querySchema[AssigneeMappingConfig]("\"assigneeMappingConfig\"").filter(_.id == lift(id))
    }
  }.transact(tr).map(_.headOption)

  override def getByBoardId(boardId: UUID): F[List[AssigneeMappingConfig]] = run {
    quote {
      querySchema[AssigneeMappingConfig]("\"assigneeMappingConfig\"").filter(
        _.boardSync == lift(boardId),
      )
    }
  }.transact(tr)

  override def deleteConfigForBoard(boardId: UUID): F[Long] = run {
    quote {
      querySchema[AssigneeMappingConfig]("\"assigneeMappingConfig\"")
        .filter(_.boardSync == lift(boardId))
        .delete
    }
  }.transact(tr)
}
