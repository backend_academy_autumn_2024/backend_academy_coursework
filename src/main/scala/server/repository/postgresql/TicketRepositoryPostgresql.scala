package server.repository.postgresql

import cats.effect.kernel.MonadCancelThrow
import io.getquill.SnakeCase
import io.getquill.doobie.DoobieContext
import cats.implicits.toFunctorOps
import doobie.Transactor
import doobie.implicits._
import cats.effect.kernel.MonadCancelThrow
import server.domain.ticket.Ticket
import server.repository.TicketRepository

import java.util.UUID

class TicketRepositoryPostgresql[F[_]: MonadCancelThrow](implicit tr: Transactor[F])
    extends TicketRepository[F] {

  private val ctx = new DoobieContext.Postgres(SnakeCase)

  import ctx._

  override def create(ticket: Ticket): F[Long] = run {
    quote {
      querySchema[Ticket]("\"ticketSync\"")
        .insertValue(lift(ticket))
    }
  }.transact(tr)

  override def get(id: UUID): F[Option[Ticket]] = run {
    quote {
      querySchema[Ticket]("\"ticketSync\"").filter(_.id == lift(id))
    }
  }.transact(tr).map(_.headOption)

  override def getByBoardId(boardId: UUID): F[List[Ticket]] = run {
    quote {
      querySchema[Ticket]("\"ticketSync\"").filter(_.boardSync == lift(boardId))
    }
  }.transact(tr)

  override def update(ticket: Ticket): F[Long] = run {
    quote {
      querySchema[Ticket]("\"ticketSync\"")
        //.updateValue(ticket)
        .filter(_.id == lift(ticket.id))
        //.upda
        .updateValue(lift(ticket))
    }
  }.transact(tr)

  override def delete(id: UUID): F[Option[Ticket]] = run {
    quote {
      querySchema[Ticket]("\"ticketSync\"")
        .filter(_.id == lift(id))
        .delete
        .returningMany(r => r)
    }
  }.transact(tr).map(_.headOption)

}
