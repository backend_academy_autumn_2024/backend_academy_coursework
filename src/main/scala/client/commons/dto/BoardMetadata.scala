package client.commons.dto

case class BoardMetadata(boardName: String, tagsOptions: List[String], statusOptions: List[String])
