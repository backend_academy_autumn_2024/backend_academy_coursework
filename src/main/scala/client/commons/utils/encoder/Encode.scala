package client.commons.utils.encoder

import io.circe.Encoder

object Encode {
  def dropNulls[A](encoder: Encoder[A]): Encoder[A] =
    encoder.mapJson(_.dropNullValues)
}
