package client.commons.utils

sealed trait ResponseParam {
  val name: String
  override def toString: String =
    this match {
      case CompositeParam(name, list) => s"$name(${list.map(_.toString).mkString(",")})"
      case SimpleParam(name)          => name
    }
}

case class CompositeParam(override val name: String, list: List[ResponseParam])
    extends ResponseParam
case class SimpleParam(override val name: String) extends ResponseParam
