package client.jb_space

import cats.effect.kernel.Async
import cats.implicits._
import client.commons.SttpResponseUtils
import client.commons.utils.{CompositeParam, ResponseParam, SimpleParam}
import client.jb_space.model.configuration.JBSpaceClientConfiguration
import client.jb_space.model.response.auth.{AuthError, AuthorizedRightResponse}
import client.jb_space.model.response.board.BoardName
import client.jb_space.model.response.issue.IssueResponse
import client.jb_space.model.response.issue.customFields.CustomFieldData
import client.jb_space.model.response.issue.status.Status
import client.jb_space.model.response.issue.tag.{Tag, TagsData}
import client.jb_space.model.response.project.{ProjectMembers, ProjectsData}
import com.softwaremill.quicklens.QuicklensWhen
import sttp.client3.circe.{asJson, asJsonAlways}
import sttp.client3.{
  ConditionalResponseAs,
  Response,
  ResponseException,
  SttpBackend,
  UriContext,
  asEither,
  basicRequest,
  fromMetadata,
}
import sttp.model.{StatusCode, Uri}
import sttp.tapir.statusCode

trait JBSpaceClient[F[_]] {
  def isApiLinkValidated(str: String): Either[String, String]
  def getAllProjects(bearerToken: String, username: String): F[ProjectsData]
  def getAllIssuesOnBoard(bearerToken: String, username: String, boardId: String): F[IssueResponse]
  def getAllTagsOnProject(bearerToken: String, username: String, projectId: String): F[TagsData]

  def getAllStatusesOnProject(
      bearerToken: String,
      username: String,
      projectId: String,
  ): F[List[Status]]
  def getCustomFieldOfIssue(
      bearerToken: String,
      username: String,
      projectId: String,
  ): F[List[CustomFieldData]]

  def getAuthorizedRightsOfToken(
      bearerToken: String,
      username: String,
  ): F[Either[AuthError, List[AuthorizedRightResponse]]]

  def getNameOfBoard(bearerToken: String, username: String, boardId: String): F[BoardName]

  def getAllMembersOfProject(
      bearerToken: String,
      username: String,
      projectId: String,
  ): F[ProjectMembers]
  //def createOrder(request: CreatePageRequest): F[PageResponse]
  //def findOrder(orderId: UUID): F[Option[OrderResponse]]

  def getRequiredRightOfToken: List[String] //TODO: not supposed to be here (maybe in service?)
}

class HttpJBSpaceClient[F[_]: Async](
    sttpBackend: SttpBackend[F, Any],
    jBSpaceClientConfiguration: JBSpaceClientConfiguration,
) extends JBSpaceClient[F] {

  override def getAllProjects(bearerToken: String, username: String): F[ProjectsData] = {
    val dataParams: List[ResponseParam] = List(
      CompositeParam(
        "data",
        List(
          SimpleParam("id"),
          SimpleParam("description"),
          SimpleParam("name"),
          CompositeParam(
            "boards",
            List(
              SimpleParam("id"),
              SimpleParam("name"),
            ),
          ),
        ),
      ),
      SimpleParam("next"),
      SimpleParam("totalCount"),
    )

    val getAllProjectsUri: Uri =
      uri"${jBSpaceClientConfiguration.baseUrlPrefix}$username${jBSpaceClientConfiguration.baseUrlPostfix}/api/http/projects"
        .addParam("$fields", dataParams.mkString(","))

    basicRequest.auth
      .bearer(bearerToken)
      .get(getAllProjectsUri)
      .response(SttpResponseUtils.unwrapResponse[F, ProjectsData])
      .send(sttpBackend)
      .flatMap(_.body)
  }
  override def getAllTagsOnProject(
      bearerToken: String,
      username: String,
      projectId: String,
  ): F[TagsData] = {
    val dataParams: List[ResponseParam] = List(
      CompositeParam(
        "data",
        List(
          SimpleParam("id"),
          SimpleParam("name"),
        ),
      ),
    )

    val getAllTagsOnProjectUri: Uri =
      uri"${jBSpaceClientConfiguration.baseUrlPrefix}$username${jBSpaceClientConfiguration.baseUrlPostfix}/api/http/projects/id:$projectId/planning/tags"
        .addParam("$fields", dataParams.mkString(","))
    //println(getAllTagsOnProjectUri)

    basicRequest.auth
      .bearer(bearerToken)
      .get(getAllTagsOnProjectUri)
      .response(SttpResponseUtils.unwrapResponse[F, TagsData])
      .send(sttpBackend)
      .flatMap(_.body)
  }

  override def getAllIssuesOnBoard(
      bearerToken: String,
      username: String,
      boardId: String,
  ): F[IssueResponse] = {
    val dataParams: List[CompositeParam] = List(
      CompositeParam(
        "data",
        List(
          SimpleParam("id"),
          CompositeParam(
            "assignee",
            List(
              SimpleParam("id"),
              SimpleParam("username"),
            ),
          ),
          SimpleParam("dueDate"),
          CompositeParam(
            "status",
            List(
              SimpleParam("id"),
              SimpleParam("name"),
            ),
          ),
          CompositeParam(
            "tags",
            List(
              SimpleParam("id"),
              SimpleParam("name"),
            ),
          ),
          SimpleParam("title"),
          SimpleParam("customFields"),
          SimpleParam("description"),
        ),
      ),
    )
    //TODO: add custom field

    val getAllIssuesOnBoardUri: Uri =
      uri"${jBSpaceClientConfiguration.baseUrlPrefix}$username${jBSpaceClientConfiguration.baseUrlPostfix}/api/http/projects/planning/boards/id:$boardId/issues"
        .addParam("$fields", dataParams.mkString(","))

    //val uriAsString = getAllIssuesOnBoardUri.toString()

    basicRequest.auth
      .bearer(bearerToken)
      .get(getAllIssuesOnBoardUri)
      .response(SttpResponseUtils.unwrapResponse[F, IssueResponse])
      .send(sttpBackend)
      .flatMap(_.body)

  }

  override def getCustomFieldOfIssue(
      bearerToken: String,
      username: String,
      projectId: String,
  ): F[List[CustomFieldData]] = {
    val dataParams: List[ResponseParam] = List(
      SimpleParam("id"),
      SimpleParam("description"),
      SimpleParam("name"),
      CompositeParam(
        "parameters",
        List(
          CompositeParam(
            "values",
            List(
              SimpleParam("id"),
              SimpleParam("value"),
            ),
          ),
        ),
      ),
    )

    val getCustomFieldOfIssueUri: Uri =
      uri"${jBSpaceClientConfiguration.baseUrlPrefix}$username${jBSpaceClientConfiguration.baseUrlPostfix}/api/http/custom-fields-v2/issueTracker:project:id:$projectId/fields"
        .addParam("$fields", dataParams.mkString(","))

    //${jBSpaceClientConfiguration.baseUrlPrefix}${username}${jBSpaceClientConfiguration.baseUrlPostfix}
    //println(getCustomFieldOfIssueUri.toString())
    basicRequest.auth
      .bearer(bearerToken)
      .get(getCustomFieldOfIssueUri)
      .response(SttpResponseUtils.unwrapResponse[F, List[CustomFieldData]])
      .send(sttpBackend)
      .flatMap(_.body)

  }

  override def getNameOfBoard(
      bearerToken: String,
      username: String,
      boardId: String,
  ): F[BoardName] = {

    val dataParams: ResponseParam = SimpleParam("name")
    val getNameOfBoardUri: Uri =
      uri"${jBSpaceClientConfiguration.baseUrlPrefix}$username${jBSpaceClientConfiguration.baseUrlPostfix}/api/http/projects/planning/boards/id:$boardId"
        .addParam("$fields", dataParams.toString)

    basicRequest.auth
      .bearer(bearerToken)
      .get(getNameOfBoardUri)
      .response(SttpResponseUtils.unwrapResponse[F, BoardName])
      .send(sttpBackend)
      .flatMap(_.body)
  }

  override def getAuthorizedRightsOfToken(
      bearerToken: String,
      username: String,
  ): F[Either[AuthError, List[AuthorizedRightResponse]]] = {
    val dataParams: List[SimpleParam] = List(
      SimpleParam("name"),
      SimpleParam("status"),
    )
    val getNameOfBoardUri: Uri =
      uri"${jBSpaceClientConfiguration.baseUrlPrefix}$username${jBSpaceClientConfiguration.baseUrlPostfix}/api/http/applications/me/authorizations/authorized-rights?contextIdentifier=global"
        .addParam("$fields", dataParams.mkString(","))

    basicRequest.auth
      .bearer(bearerToken)
      .get(getNameOfBoardUri)
      .response(
        asEither(
          SttpResponseUtils.unwrapResponse[F, AuthError],
          SttpResponseUtils.unwrapResponse[F, List[AuthorizedRightResponse]],
        ),
      )
      .send(sttpBackend)
      .flatMap { response =>
        response.body match {
          case Right(value) => value.map(Right(_))
          case Left(error)  => error.map(Left(_))
        }
      }
  }

  override def getAllStatusesOnProject(
      bearerToken: String,
      username: String,
      projectId: String,
  ): F[List[Status]] = {
    val dataParams: List[ResponseParam] = List(
      SimpleParam("id"),
      SimpleParam("name"),
    )

    val getNameOfBoardUri: Uri =
      uri"${jBSpaceClientConfiguration.baseUrlPrefix}$username${jBSpaceClientConfiguration.baseUrlPostfix}/api/http/projects/id:$projectId/planning/issues/statuses"
        .addParam("$fields", dataParams.mkString(","))

    basicRequest.auth
      .bearer(bearerToken)
      .get(getNameOfBoardUri)
      .response(SttpResponseUtils.unwrapResponse[F, List[Status]])
      .send(sttpBackend)
      .flatMap(_.body)
  }

  override def getAllMembersOfProject(
      bearerToken: String,
      username: String,
      projectId: String,
  ): F[ProjectMembers] = {
    val dataParams: List[ResponseParam] = List(
      CompositeParam(
        "data",
        List(
          SimpleParam("id"),
          SimpleParam("name"),
          SimpleParam("username"),
          CompositeParam(
            "emails",
            List(
              SimpleParam("email"),
            ),
          ),
        ),
      ),
    )

    val getNameOfBoardUri: Uri =
      uri"${jBSpaceClientConfiguration.baseUrlPrefix}$username${jBSpaceClientConfiguration.baseUrlPostfix}/api/http/projects/id:$projectId/access/member-profiles"
        .addParam("includingAdmins", true.toString)
        .addParam("$fields", dataParams.mkString(","))

    basicRequest.auth
      .bearer(bearerToken)
      .get(getNameOfBoardUri)
      .response(SttpResponseUtils.unwrapResponse[F, ProjectMembers])
      .send(sttpBackend)
      .flatMap(_.body)
  }

  override def getRequiredRightOfToken: List[String] =
    jBSpaceClientConfiguration.requiredTokenRights

  override def isApiLinkValidated(str: String): Either[String, String] =
    str match {
      case jBSpaceClientConfiguration.baseUrlRegex(prefix, username, postfix, uselessEnding) =>
        Right(username)
      case _ => Left("link is invalid")
    }
}
