package client.jb_space

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import client.commons.SttpResponseUtils
import _root_.client.jb_space.model.configuration.JBSpaceClientConfiguration
import _root_.client.jb_space.model.response.project.{ProjectResponse, ProjectsData}
import _root_.client.notion.model.configuration.NotionClientConfiguration
import _root_.client.notion.model.request.CreatePageRequest
import _root_.client.notion.model.response.{PageResponse, UserInfoResponse}
import client.jb_space.model.response.issue
import client.jb_space.model.response.issue.IssueResponse
import com.typesafe.config.{Config, ConfigFactory}
import io.circe.Decoder.Result
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, HCursor, jawn, parser}
import org.asynchttpclient.DefaultAsyncHttpClient
import sttp.client3
import sttp.client3.asynchttpclient.cats.AsyncHttpClientCatsBackend
import sttp.client3.{
  HttpClientSyncBackend,
  Response,
  ResponseAs,
  SttpBackend,
  UriContext,
  asString,
  basicRequest,
}
import sttp.model.Uri

import java.util.UUID
import io.circe.parser.decode

object SpaceTestMain extends App {

  val config: Config = ConfigFactory.load()
  val jBSpaceClientConfiguration: JBSpaceClientConfiguration =
    JBSpaceClientConfiguration.load(config)

  val idOfProject: String = "VE1GS0VJoMm" // - id of a project
  val idOfBoard: String = "3wIqCT3q6bFS" // - id of a board

//  val jsonExample = """{
//                      |    "data": [
//                      |        {
//                      |            "id": "1eBx1u0UC9JW",
//                      |            "assignee": null,
//                      |            "status": {
//                      |                "name": "Open"
//                      |            },
//                      |            "dueDate": null,
//                      |            "tags": [],
//                      |            "title": "Welcome to Space 🚀",
//                      |            "description": "Hi there! Complete these issues to learn how to use Space. When you're done, simply delete them.\n\nWant to learn more about Space? Launch a demo project to try all of Space’s features in action or [watch the product video](https://youtu.be/utW0-mMYUL4?si=CZTpG9dfOlXrhySu).",
//                      |            "customFields": {
//                      |                "Priority": {
//                      |                    "className": "EnumCFValue",
//                      |                    "value": {
//                      |                        "id": "1oTaWL1Y9jws",
//                      |                        "value": "Normal",
//                      |                        "principal": {
//                      |                            "className": "CUserPrincipalDetails",
//                      |                            "user": {
//                      |                                "id": "22pxpR0Bw9O2"
//                      |                            }
//                      |                        }
//                      |                    }
//                      |                }
//                      |            }
//                      |        },
//                      |        {
//                      |            "id": "1bgewl06ArUm",
//                      |            "assignee": {
//                      |                "id": "22pxpR0Bw9O2",
//                      |                "$errors": [
//                      |                    {
//                      |                        "className": "HA_InlineError.InaccessibleFields",
//                      |                        "fields": [
//                      |                            "name"
//                      |                        ],
//                      |                        "message": "Could not access \"TD_MemberProfile\" object. The object may not exist or required permissions may be missing."
//                      |                    }
//                      |                ]
//                      |            },
//                      |            "status": {
//                      |                "name": "Done"
//                      |            },
//                      |            "dueDate": null,
//                      |            "tags": [],
//                      |            "title": "Create an issue",
//                      |            "description": "In Space, issues can communicate with other Space entities such as chats, repositories, applications, and other resources to keep you updated on all related changes and discussions.\n\nLook through the Subitems below to learn some issue tracking best practices and read more about Space issues in the [Space Documentation →](https://www.jetbrains.com/help/space/issue-tracker.html)",
//                      |            "customFields": {
//                      |                "Priority": {
//                      |                    "className": "EnumCFValue",
//                      |                    "value": {
//                      |                        "id": "1oTaWL1Y9jws",
//                      |                        "value": "Normal",
//                      |                        "principal": {
//                      |                            "className": "CUserPrincipalDetails",
//                      |                            "user": {
//                      |                                "id": "22pxpR0Bw9O2"
//                      |                            }
//                      |                        }
//                      |                    }
//                      |                }
//                      |            }
//                      |        },
//                      |        {
//                      |            "id": "SRzPv0Kap6f",
//                      |            "assignee": null,
//                      |            "status": {
//                      |                "name": "Open"
//                      |            },
//                      |            "dueDate": null,
//                      |            "tags": [],
//                      |            "title": "Explore Space Automation",
//                      |            "description": "* Use Space Automation to minimize repetitive tasks and increase efficiency.\n* Achieve a stable main branch and better code quality by [configuring quality gates for merge requests](https://www.jetbrains.com/help/space/branch-and-merge-restrictions.html#quality-gates-for-merge-requests).\n* Make the release lifecycle of your product more transparent with Space deployments.\n* Subscribe to job notifications to receive alerts whenever a job fails or succeeds.\n\n⚡️ ***Pro tip:*** *Use the native integration with TeamCity to configure quality gates based on TeamCity build status for merge requests to deliver better code and make your main green. [Watch how it works in the video](https://youtu.be/DfG2FYsH2yk?si=V6B8JroJUgOw54Ik).*",
//                      |            "customFields": {
//                      |                "Priority": {
//                      |                    "className": "EnumCFValue",
//                      |                    "value": {
//                      |                        "id": "1oTaWL1Y9jws",
//                      |                        "value": "Normal",
//                      |                        "principal": {
//                      |                            "className": "CUserPrincipalDetails",
//                      |                            "user": {
//                      |                                "id": "22pxpR0Bw9O2"
//                      |                            }
//                      |                        }
//                      |                    }
//                      |                }
//                      |            }
//                      |        },
//                      |        {
//                      |            "id": "4acMtl3ExhGQ",
//                      |            "assignee": null,
//                      |            "status": {
//                      |                "name": "Open"
//                      |            },
//                      |            "dueDate": null,
//                      |            "tags": [],
//                      |            "title": "Streamline collaboration with Space chats and smart alerts",
//                      |            "description": "We believe that chat-based communication helps you achieve faster interactions.\n* Chat in public and private channels or via DMs.\n* Receive personalized notifications about the events that matter to you.\n* Review code, discuss issues, and collaborate in place.\n* Continue conversations on the go with the Space mobile app for iOS and Android.\n\n⚡️ ***Pro tip:*** *Want to share a code snippet? Highlight a portion of code and send it directly to a chat or channel.*\n\n![share-code-snippet.png](/d/2FVyRa1T0pna?f=0)",
//                      |            "customFields": {
//                      |                "Priority": {
//                      |                    "className": "EnumCFValue",
//                      |                    "value": {
//                      |                        "id": "1oTaWL1Y9jws",
//                      |                        "value": "Normal",
//                      |                        "principal": {
//                      |                            "className": "CUserPrincipalDetails",
//                      |                            "user": {
//                      |                                "id": "22pxpR0Bw9O2"
//                      |                            }
//                      |                        }
//                      |                    }
//                      |                }
//                      |            }
//                      |        },
//                      |        {
//                      |            "id": "q3pcs1Z05b5",
//                      |            "assignee": {
//                      |                "id": "22pxpR0Bw9O2",
//                      |                "$errors": [
//                      |                    {
//                      |                        "className": "HA_InlineError.InaccessibleFields",
//                      |                        "fields": [
//                      |                            "name"
//                      |                        ],
//                      |                        "message": "Could not access \"TD_MemberProfile\" object. The object may not exist or required permissions may be missing."
//                      |                    }
//                      |                ]
//                      |            },
//                      |            "status": {
//                      |                "name": "Open"
//                      |            },
//                      |            "dueDate": null,
//                      |            "tags": [],
//                      |            "title": "Helpful Resources",
//                      |            "description": "You’re all set up for your Space journey. In case you get lost on your way, here are some helpful resources:\n* Read our [documentation](https://www.jetbrains.com/help/space) to dive into specific features.\n* Check out the [Space blog](https://blog.jetbrains.com/space) for useful tips and the latest news.\n* Learn about new features on the [What’s new page](https://www.jetbrains.com/space/whatsnew).\n* Watch our [video guides](https://www.youtube.com/watch?v=6wy53t9Utb0&list=PLQ176FUIyIUY0UPgxq2qYytK5H-IoWWFw).\n\n✨ ***ProTip:*** *[Schedule a demo](https://calendly.com/leonid-zalog/demo-session-for-your-team?utm_content=product) with the Space team to cover all the bases, scenarios, and questions that are important to you.*",
//                      |            "customFields": {
//                      |                "Priority": {
//                      |                    "className": "EnumCFValue",
//                      |                    "value": {
//                      |                        "id": "1oTaWL1Y9jws",
//                      |                        "value": "Normal",
//                      |                        "principal": {
//                      |                            "className": "CUserPrincipalDetails",
//                      |                            "user": {
//                      |                                "id": "22pxpR0Bw9O2"
//                      |                            }
//                      |                        }
//                      |                    }
//                      |                }
//                      |            }
//                      |        },
//                      |        {
//                      |            "id": "4DsVa93HrJXl",
//                      |            "assignee": {
//                      |                "id": "22pxpR0Bw9O2",
//                      |                "$errors": [
//                      |                    {
//                      |                        "className": "HA_InlineError.InaccessibleFields",
//                      |                        "fields": [
//                      |                            "name"
//                      |                        ],
//                      |                        "message": "Could not access \"TD_MemberProfile\" object. The object may not exist or required permissions may be missing."
//                      |                    }
//                      |                ]
//                      |            },
//                      |            "status": {
//                      |                "name": "Open"
//                      |            },
//                      |            "dueDate": {
//                      |                "iso": "2023-10-24",
//                      |                "year": 2023,
//                      |                "month": 10,
//                      |                "day": 24
//                      |            },
//                      |            "tags": [
//                      |                {
//                      |                    "id": "3VYoDR3K9ut6",
//                      |                    "name": "devops"
//                      |                },
//                      |                {
//                      |                    "id": "3qUKaI2NvQ6u",
//                      |                    "name": "frontend"
//                      |                }
//                      |            ],
//                      |            "title": "find how get issue board through jetbrains space api",
//                      |            "description": "hello it's issue from jetbrains space",
//                      |            "customFields": {
//                      |                "Priority": {
//                      |                    "className": "EnumCFValue",
//                      |                    "value": {
//                      |                        "id": "1oTaWL1Y9jws",
//                      |                        "value": "Normal",
//                      |                        "principal": {
//                      |                            "className": "CUserPrincipalDetails",
//                      |                            "user": {
//                      |                                "id": "22pxpR0Bw9O2"
//                      |                            }
//                      |                        }
//                      |                    }
//                      |                }
//                      |            }
//                      |        },
//                      |        {
//                      |            "id": "sLHGa1KGg58",
//                      |            "assignee": null,
//                      |            "status": {
//                      |                "name": "In progress"
//                      |            },
//                      |            "dueDate": null,
//                      |            "tags": [],
//                      |            "title": "fdsafdsa(какое-то изменение)",
//                      |            "description": null,
//                      |            "customFields": {
//                      |                "Priority": {
//                      |                    "className": "EnumCFValue",
//                      |                    "value": {
//                      |                        "id": "1oTaWL1Y9jws",
//                      |                        "value": "Normal",
//                      |                        "principal": {
//                      |                            "className": "CUserPrincipalDetails",
//                      |                            "user": {
//                      |                                "id": "22pxpR0Bw9O2"
//                      |                            }
//                      |                        }
//                      |                    }
//                      |                }
//                      |            }
//                      |        },
//                      |        {
//                      |            "id": "2s8a4I3omzuL",
//                      |            "assignee": null,
//                      |            "status": {
//                      |                "name": "In progress"
//                      |            },
//                      |            "dueDate": null,
//                      |            "tags": [],
//                      |            "title": "fdsafdsafsda",
//                      |            "description": "",
//                      |            "customFields": {
//                      |                "Priority": {
//                      |                    "className": "EnumCFValue",
//                      |                    "value": {
//                      |                        "id": "1oTaWL1Y9jws",
//                      |                        "value": "Normal",
//                      |                        "principal": {
//                      |                            "className": "CUserPrincipalDetails",
//                      |                            "user": {
//                      |                                "id": "22pxpR0Bw9O2"
//                      |                            }
//                      |                        }
//                      |                    }
//                      |                }
//                      |            }
//                      |        },
//                      |        {
//                      |            "id": "ibM4G0Xi1RF",
//                      |            "assignee": null,
//                      |            "status": {
//                      |                "name": "In progress"
//                      |            },
//                      |            "dueDate": null,
//                      |            "tags": [],
//                      |            "title": "rewqrewqrweq",
//                      |            "description": "",
//                      |            "customFields": {
//                      |                "Priority": {
//                      |                    "className": "EnumCFValue",
//                      |                    "value": {
//                      |                        "id": "1oTaWL1Y9jws",
//                      |                        "value": "Normal",
//                      |                        "principal": {
//                      |                            "className": "CUserPrincipalDetails",
//                      |                            "user": {
//                      |                                "id": "22pxpR0Bw9O2"
//                      |                            }
//                      |                        }
//                      |                    }
//                      |                }
//                      |            }
//                      |        },
//                      |        {
//                      |            "id": "tUGMW1QAwJ3",
//                      |            "assignee": null,
//                      |            "status": {
//                      |                "name": "Open"
//                      |            },
//                      |            "dueDate": {
//                      |                "iso": "2023-10-12",
//                      |                "year": 2023,
//                      |                "month": 10,
//                      |                "day": 12
//                      |            },
//                      |            "tags": [
//                      |                {
//                      |                    "id": "261Cxt0ylX2L",
//                      |                    "name": "backend"
//                      |                }
//                      |            ],
//                      |            "title": "привет",
//                      |            "description": "какой-то пример описания гавна",
//                      |            "customFields": {
//                      |                "Priority": {
//                      |                    "className": "EnumCFValue",
//                      |                    "value": {
//                      |                        "id": "4eeHXM2wI7Yu",
//                      |                        "value": "Critical",
//                      |                        "principal": {
//                      |                            "className": "CUserPrincipalDetails",
//                      |                            "user": {
//                      |                                "id": "22pxpR0Bw9O2"
//                      |                            }
//                      |                        }
//                      |                    }
//                      |                }
//                      |            }
//                      |        },
//                      |        {
//                      |            "id": "1YWxQ0cmGDy",
//                      |            "assignee": null,
//                      |            "status": {
//                      |                "name": "Open"
//                      |            },
//                      |            "dueDate": null,
//                      |            "tags": [],
//                      |            "title": "1-st",
//                      |            "description": "",
//                      |            "customFields": {
//                      |                "Priority": {
//                      |                    "className": "EnumCFValue",
//                      |                    "value": {
//                      |                        "id": "1oTaWL1Y9jws",
//                      |                        "value": "Normal",
//                      |                        "principal": {
//                      |                            "className": "CUserPrincipalDetails",
//                      |                            "user": {
//                      |                                "id": "22pxpR0Bw9O2"
//                      |                            }
//                      |                        }
//                      |                    }
//                      |                }
//                      |            }
//                      |        },
//                      |        {
//                      |            "id": "1axy8e0NvVrM",
//                      |            "assignee": null,
//                      |            "status": {
//                      |                "name": "Open"
//                      |            },
//                      |            "dueDate": null,
//                      |            "tags": [],
//                      |            "title": "2nd",
//                      |            "description": "",
//                      |            "customFields": {
//                      |                "Priority": {
//                      |                    "className": "EnumCFValue",
//                      |                    "value": {
//                      |                        "id": "1oTaWL1Y9jws",
//                      |                        "value": "Normal",
//                      |                        "principal": {
//                      |                            "className": "CUserPrincipalDetails",
//                      |                            "user": {
//                      |                                "id": "22pxpR0Bw9O2"
//                      |                            }
//                      |                        }
//                      |                    }
//                      |                }
//                      |            }
//                      |        },
//                      |        {
//                      |            "id": "43EoaB2UC4vS",
//                      |            "assignee": null,
//                      |            "status": {
//                      |                "name": "Open"
//                      |            },
//                      |            "dueDate": null,
//                      |            "tags": [],
//                      |            "title": "3rd",
//                      |            "description": "",
//                      |            "customFields": {
//                      |                "Priority": {
//                      |                    "className": "EnumCFValue",
//                      |                    "value": {
//                      |                        "id": "1oTaWL1Y9jws",
//                      |                        "value": "Normal",
//                      |                        "principal": {
//                      |                            "className": "CUserPrincipalDetails",
//                      |                            "user": {
//                      |                                "id": "22pxpR0Bw9O2"
//                      |                            }
//                      |                        }
//                      |                    }
//                      |                }
//                      |            }
//                      |        }
//                      |    ]
//                      |}""".stripMargin
//
//  implicit val jsonParser: Decoder[IssueResponse] = deriveDecoder[IssueResponse]
//  val result = decode[IssueResponse](jsonExample) //.//parse(jsonString)
//
//  val anotherJson = """{
//                      |    "data": [
//                      |        {
//                      |            "id": "1eBx1u0UC9JW",
//                      |             "name": "someName"
//                      |        },
//                      |        {
//                      |            "id": "1eBx1ufdsa0UC9JW",
//                      |             "name": "someNfdsaame"
//                      |        }
//                      |     ]
//                      |}
//                      |             """.stripMargin
//
//  case class BaseClass(id: String, name: String)
//  case class TopClass(data: List[BaseClass])
//
//  implicit val decoder1: Decoder[BaseClass] = deriveDecoder[BaseClass]
//
//  implicit val decoder: Decoder[TopClass] = deriveDecoder[TopClass]
//
//  val result1 = decode[TopClass](anotherJson)
//
//  println(result1)

  val res: IO[Unit] = for {
    sttpBackend <- AsyncHttpClientCatsBackend[IO]()
    client: JBSpaceClient[IO] = new HttpJBSpaceClient[IO](sttpBackend, jBSpaceClientConfiguration)
    result <- client.getAllMembersOfProject(
      jBSpaceClientConfiguration.myBearerToken,
      "fdsa",
      idOfProject,
    )

    //result <- client.getCustomFieldOfIssue(jBSpaceClientConfiguration.myBearerToken, idOfProject)
    _ <- IO.println(result)
  } yield (IO.println(result))

  val someResult = res.unsafeRunSync()

  //println(res.unsafeRunAsync())
//  val client: JBSpaceClient[IO] = new HttpJBSpaceClient(sttpBackend, jBSpaceClientConfiguration)
//
//  val result: IO[ProjectsData] = client.getAllProjects(jBSpaceClientConfiguration.myBearerToken)
//  for {
//    projects <- result
//    _ = println(projects.data)
//  } yield ()
//  println(result.unsafeRunSync())
//  val request = basicRequest.get(getAllProjects)
//    .auth.bearer(jBSpaceClientConfiguration.myBearerToken)
//    .response(SttpResponseUtils.unwrapResponse[IO, ProjectsData])
//  //val some = parser.parse()
//  import cats.implicits._
//
//  //val response = request.send(backend)
//  val response: client3.Identity[Response[IO[ProjectsData]]] = request.send(backend)
//
//  //println(response.body)
//  println(response.flatMap(_.body).unsafeRunSync())
  //val result: UserInfoResponse = response.unsafeRunSync()

  //println(response.header("Content-Length"))
  //println(result)

}
