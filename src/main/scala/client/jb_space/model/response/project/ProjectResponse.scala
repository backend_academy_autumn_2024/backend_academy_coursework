package client.jb_space.model.response.project

import client.jb_space.model.response.board.BoardResponse
import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

case class ProjectResponse(
    id: String,
    description: String,
    name: String,
    boards: List[BoardResponse],
)

object ProjectResponse {
  implicit val projectResponseDecoder: Decoder[ProjectResponse] = deriveDecoder[ProjectResponse]
}
