package client.jb_space.model.response.project

import client.jb_space.model.response.board.BoardResponse
import client.jb_space.model.response.issue.Assignee
import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

case class ProjectMembers(data: List[Assignee])

object ProjectMembers {
  implicit val projectMembersDecoder: Decoder[ProjectMembers] = deriveDecoder[ProjectMembers]
}
