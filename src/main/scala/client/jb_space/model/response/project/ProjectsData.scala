package client.jb_space.model.response.project

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

case class ProjectsData(
    data: List[ProjectResponse],
    next: Int, //TODO: idk what this mean and why i use this
    totalCount: Int,
)

object ProjectsData {
  implicit val projectsDataDecoder: Decoder[ProjectsData] = deriveDecoder[ProjectsData]
}
