package client.jb_space.model.response.auth

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

case class AuthorizedRightResponse(name: String, status: String)

object AuthorizedRightResponse {
  implicit val autorizedRightResponseDecoder: Decoder[AuthorizedRightResponse] =
    deriveDecoder[AuthorizedRightResponse]
}
