package client.jb_space.model.response.auth

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

case class AuthError(error: String, error_description: String)

object AuthError {
  implicit val autorizedRightResponseDecoder: Decoder[AuthError] = deriveDecoder[AuthError]
}
