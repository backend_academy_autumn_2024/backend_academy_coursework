package client.jb_space.model.response.board

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

case class BoardInfo(columns: String, boardInfo: BoardInfo)
object BoardInfo {
  implicit val boardResponseDecoder: Decoder[BoardInfo] = deriveDecoder[BoardInfo]
}
