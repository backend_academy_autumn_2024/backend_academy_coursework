package client.jb_space.model.response.board

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

case class BoardName(name: String)

object BoardName {
  implicit val boardResponseDecoder: Decoder[BoardName] = deriveDecoder[BoardName]
}
