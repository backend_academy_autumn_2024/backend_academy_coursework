package client.jb_space.model.response.board

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder
import server.common.tethys.TethysInstances
import sttp.tapir.Schema
import tethys.derivation.semiauto.{jsonReader, jsonWriter}
import tethys.{JsonReader, JsonWriter}

case class BoardResponse(
    id: String, //aka BoardRecord
    name: String,
    //boardInfo: BoardInfo
)
object BoardResponse extends TethysInstances {
  implicit val orderResponseReader: JsonReader[BoardResponse] = jsonReader

  implicit val orderResponseWriter: JsonWriter[BoardResponse] = jsonWriter

  implicit val orderResponseSchema: Schema[BoardResponse] = Schema.derived
    .description("Заказ")

  implicit val boardResponseDecoder: Decoder[BoardResponse] = deriveDecoder[BoardResponse]
}
