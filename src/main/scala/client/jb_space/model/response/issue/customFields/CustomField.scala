package client.jb_space.model.response.issue.customFields

import io.circe.Decoder.Result
import io.circe.{Decoder, DecodingFailure, HCursor}
import io.circe.generic.semiauto.deriveDecoder

sealed trait CustomField {
  val className: String
  val name: String
}

object CustomField {
  implicit val customFieldDecoder: Decoder[CustomField] = deriveDecoder[CustomField]
  def apply(name: String, className: String, hCursor: HCursor): Result[CustomField] =
    className match {
      case EnumCFValue.className => EnumCFValue.applyFromHCursor(name, hCursor)
      case unknownClassName =>
        Left(DecodingFailure(s"unknownClassName: $unknownClassName", hCursor.history))
    }
}

sealed trait CustomFieldProducer {
  def applyFromHCursor(name: String, HCursor: HCursor): Result[CustomField]
}

case class EnumCFValue(
    override val className: String = EnumCFValue.className,
    override val name: String,
    idOfValue: String,
    value: String,
) extends CustomField
object EnumCFValue extends CustomFieldProducer {

  val className: String = "EnumCFValue"

  override def applyFromHCursor(name: String, hCursor: HCursor): Result[CustomField] = {
    val value = hCursor.downField("value")
    val result: Result[EnumCFValue] = for {
      id <- value
        .get[String]("id")
        .left
        .map(e => DecodingFailure("Failed to decode 'id' field", e.history))
      v <- value
        .get[String]("value")
        .left
        .map(e => DecodingFailure("Failed to decode 'value' field", e.history))
    } yield EnumCFValue(idOfValue = id, name = name, value = v)
    result
  }
}
