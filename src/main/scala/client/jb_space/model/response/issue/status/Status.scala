package client.jb_space.model.response.issue.status

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

case class Status(id: String, name: String)

object Status {
  implicit val statusDecoder: Decoder[Status] = deriveDecoder[Status]
}
