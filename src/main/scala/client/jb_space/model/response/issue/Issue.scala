package client.jb_space.model.response.issue

import client.jb_space.model.response.issue.customFields.CustomField
import client.jb_space.model.response.issue.status.Status
import io.circe.Decoder.Result
import io.circe.{Decoder, DecodingFailure, HCursor}
import io.circe.generic.semiauto.deriveDecoder
import tag.Tag

import java.time.LocalDate

case class Issue(
    id: String,
    assignee: Option[Assignee],
    dueDate: Option[LocalDate],
    status: Status,
    tags: List[Tag],
    title: String,
    description: Option[String],
    //customFields: List[CustomField],
)

object Issue {
  implicit val issueDecoder: Decoder[Issue] = deriveDecoder[Issue]

  implicit val localDate: Decoder[LocalDate] = new Decoder[LocalDate] {
    override def apply(c: HCursor): Result[LocalDate] =
      for {
        time <- c.downField("iso").as[String]
      } yield (LocalDate.parse(time))
  }

  implicit val customFieldsDecoder: Decoder[List[CustomField]] = new Decoder[List[CustomField]] {
    override def apply(c: HCursor): Result[List[CustomField]] =
      c.keys
        .map { keyList =>
          val result = for {
            customFieldName <- keyList
            validHCursor <- c.downField(customFieldName).success
            validClassName <- validHCursor.get[String]("className").toOption
            validCustomField <- CustomField
              .apply(customFieldName, validClassName, validHCursor)
              .toOption
          } yield validCustomField
          result.toList
        }
        .toRight(
          DecodingFailure("Custom fields decoding failure", c.history),
        )
  }

}
