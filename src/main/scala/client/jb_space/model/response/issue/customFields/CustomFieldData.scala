package client.jb_space.model.response.issue.customFields

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

case class CustomFieldData(
    id: String,
    name: String,
    description: Option[String],
    parameters: CustomField,
)

object CustomFieldData {
  implicit val customFieldDataDecoder: Decoder[CustomFieldData] = deriveDecoder[CustomFieldData]
}
