package client.jb_space.model.response.issue

import io.circe.Decoder.Result
import io.circe.{Decoder, HCursor}
import io.circe.generic.semiauto.deriveDecoder

case class IssueResponse(data: List[Issue])

object IssueResponse {
  implicit val issueResponseDecoder: Decoder[IssueResponse] = deriveDecoder[IssueResponse]

  //new Decoder[IssueResponse] {
//    override def apply(c: HCursor): Result[IssueResponse] = {
//      val decodeClipsParam = Decoder[List[Issue]].prepare(
//        _.downField("data")
//      )
//      val parsed = decodeClipsParam.apply(c)
//      val data = c.downField("data")
//      println(parsed)
//      null
//    }
//  }
}
