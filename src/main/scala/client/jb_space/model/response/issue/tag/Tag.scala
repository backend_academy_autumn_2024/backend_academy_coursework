package client.jb_space.model.response.issue.tag

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

case class Tag(id: String, name: String)

object Tag {
  implicit val tagDecoder: Decoder[Tag] = deriveDecoder[Tag]
}
