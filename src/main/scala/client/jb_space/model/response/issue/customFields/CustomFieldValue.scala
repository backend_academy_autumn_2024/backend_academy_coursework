package client.jb_space.model.response.issue.customFields

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

case class CustomFieldValue(value: String, id: String)

object CustomFieldValue {
  implicit val customFieldValueDecoder: Decoder[CustomFieldValue] = deriveDecoder[CustomFieldValue]
}
