package client.jb_space.model.response.issue.tag

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

case class TagsData(data: List[Tag])

object TagsData {
  implicit val tagsDataDecoder: Decoder[TagsData] = deriveDecoder[TagsData]
}
