package client.jb_space.model.response.issue

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

case class Assignee(
    id: String,
    username: String,
    name: Option[ProfileName],
    emails: Option[List[Email]],
)
case class ProfileName(firstName: String, lastName: String)
case class Email(email: String)
object Assignee {
  implicit val assigneeDecoder: Decoder[Assignee] = deriveDecoder[Assignee]
  implicit val profileNameDecoder: Decoder[ProfileName] = deriveDecoder[ProfileName]
  implicit val emailDecoder: Decoder[Email] = deriveDecoder[Email]
}
