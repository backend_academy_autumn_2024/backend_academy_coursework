package client.jb_space.model.request.issue

import client.jb_space.model.response.issue.customFields.CustomFieldValue

case class CustomFieldRequest(fieldId: String, className: String, value: CustomFieldValue)
