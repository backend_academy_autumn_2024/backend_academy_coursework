package client.jb_space.model.request

import client.jb_space.model.response.issue.customFields.CustomField

import java.time.LocalDate

case class UpdateJSpaceIssueRequest(
    title: String,
    description: Option[String],
    assignee: Option[String],
    status: String,
    dueDate: LocalDate,
    customFields: List[CustomField],
)
