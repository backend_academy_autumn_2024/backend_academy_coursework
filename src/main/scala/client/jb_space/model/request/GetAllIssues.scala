package client.jb_space.model.request

import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder

import java.util.UUID

final case class GetAllIssues(petId: UUID)

object GetAllIssues {
  implicit val createPageRequestEncoder: Encoder[GetAllIssues] =
    deriveEncoder[GetAllIssues]

}
