package client.jb_space.model.configuration

import com.typesafe.config.Config
import net.ceedubs.ficus.Ficus.toFicusConfig
import net.ceedubs.ficus.readers.ValueReader

import scala.concurrent.duration.{FiniteDuration, SECONDS}
import scala.jdk.CollectionConverters.CollectionHasAsScala
import scala.util.matching.Regex

case class JBSpaceClientConfiguration(
    baseUrl: String,
    baseUrlPrefix: String,
    baseUrlPostfix: String,
    timeout: FiniteDuration,
    myBearerToken: String,
    requiredTokenRights: List[String],
    baseUrlRegex: Regex,
)

object JBSpaceClientConfiguration {
  def load(config: Config): JBSpaceClientConfiguration =
    config.as[JBSpaceClientConfiguration]("jb_space.client")

  private implicit val petClientConfigurationReader: ValueReader[JBSpaceClientConfiguration] =
    ValueReader.relative { config =>
      val prefix = config.getString("base-url.prefix")
      val postfix = config.getString("base-url.postfix")
      val regexForBaseUrl = s"($prefix)(.*?)($postfix)(.*?)".r

      JBSpaceClientConfiguration(
        s"${config.getString("base-url.prefix")}yupichkin${config.getString("base-url.postfix")}",
        timeout = FiniteDuration.apply(config.getLong("timeout"), SECONDS),
        myBearerToken = config.getString("headers.my-bearer-token"),
        requiredTokenRights =
          config.getList("required-grants").unwrapped().asScala.map(_.toString).toList,
        baseUrlRegex = regexForBaseUrl,
        baseUrlPrefix = prefix,
        baseUrlPostfix = postfix,
      )
    }
}
