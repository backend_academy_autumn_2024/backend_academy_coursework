package client.notion

import cats.effect.{IO, Ref, Sync}
import cats.effect.kernel.Async
import cats.implicits._
import client.commons.SttpResponseUtils
import client.notion.model.configuration.NotionClientConfiguration
import client.notion.model.request.CreatePageRequest
import client.notion.model.request.createDatabase.CreateDatabaseInPageRequest
import client.notion.model.request.createIssue.{
  CreateIssueInDatabaseRequest,
  NotionPage,
  NotionPageWithMetadata,
}
import client.notion.model.request.updatePage.UpdatePageInDatabaseRequest
import client.notion.model.response.members.NotionMembers
import client.notion.model.response.{NotionPageResponse, PageResponse}
import org.asynchttpclient.DefaultAsyncHttpClient
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jFactory
import sttp.client3.{Response, asString}
import sttp.client3.asynchttpclient.cats.AsyncHttpClientCatsBackend
//import ru.tinkoff.petstore.client.order.model.configuration.OrderClientConfiguration
//import ru.tinkoff.petstore.client.order.model.request.CreateOrderRequest
//import ru.tinkoff.petstore.client.order.model.response.OrderResponse
//import ru.tinkoff.petstore.commons.SttpResponseUtils
import sttp.client3.circe._
import sttp.client3.{SttpBackend, UriContext, basicRequest}
import sttp.model.Uri

import java.util.UUID

trait NotionClient[F[_]] {
  def createDatabaseInPage(
      bearerToken: String,
      createDatabaseInPageRequest: CreateDatabaseInPageRequest,
  ): F[PageResponse]
  def createPageInDatabase(
      bearerToken: String,
      createIssueInDatabaseRequest: CreateIssueInDatabaseRequest,
  ): F[NotionPageResponse]

  def updatePageInDatabase(
      bearerToken: String,
      updatePageInDatabaseRequest: UpdatePageInDatabaseRequest,
  ): F[NotionPageResponse]

  def getMembersOfNotionTeamSpace(bearerToken: String): F[NotionMembers]

  //def getAllUsersOfWorkspace(bearerToken: String): F[]
  //def addRelationPropertyInDatabase(bearerToken: String, )
}

class HttpNotionClient[F[_]: Async](
    sttpBackend: SttpBackend[F, Any],
    notionClientConfiguration: NotionClientConfiguration,
) extends NotionClient[F] {
  override def createDatabaseInPage(
      bearerToken: String,
      createDatabaseInPageRequest: CreateDatabaseInPageRequest,
  ): F[PageResponse] = {
    val createDatabaseInPageUri: Uri =
      uri"${notionClientConfiguration.baseUrl}/v1/databases/"
//    println(CreateDatabaseInPageRequest.textEncoder.apply(createDatabaseInPageRequest.title.head.text))
//
//    println(CreateDatabaseInPageRequest.titleEncoder.apply(createDatabaseInPageRequest.title.head))
//    println(CreateDatabaseInPageRequest.pageParentEncoder.apply(createDatabaseInPageRequest.parent))
//    println(CreateDatabaseInPageRequest.propertiesOfDbEncoder.apply(createDatabaseInPageRequest.properties))
    //println(CreateDatabaseInPageRequest.createDatabaseInPageRequestEncoder.apply(createDatabaseInPageRequest))
    basicRequest.auth
      .bearer(bearerToken)
      .header("Notion-Version", notionClientConfiguration.notionVersion)
      .post(createDatabaseInPageUri)
      .body(createDatabaseInPageRequest)
      .response(SttpResponseUtils.unwrapResponse[F, PageResponse])
      .readTimeout(notionClientConfiguration.timeout)
      .send(sttpBackend)
      .flatMap(_.body) //returns id of created database
  }

  //override def createIssueInDatabase(bearerToken: String, databaseId: String): F[Either[String, String]] = ???

  override def createPageInDatabase(
      bearerToken: String,
      createIssueInDatabaseRequest: CreateIssueInDatabaseRequest,
  ): F[NotionPageResponse] = {
    val createIssueInDatabaseUri: Uri =
      uri"${notionClientConfiguration.baseUrl}/v1/pages/"

//    println(CreateIssueInDatabaseRequest.createIssueInDatabaseRequestEncoder.apply(createIssueInDatabaseRequest))
//    val uiopui = CreateIssueInDatabaseRequest.createIssueInDatabaseRequestEncoder.apply(createIssueInDatabaseRequest)
    basicRequest.auth
      .bearer(bearerToken)
      .header("Notion-Version", notionClientConfiguration.notionVersion)
      .post(createIssueInDatabaseUri)
      .body(createIssueInDatabaseRequest)
      .response(SttpResponseUtils.unwrapResponse[F, NotionPageResponse])
      .readTimeout(notionClientConfiguration.timeout)
      .send(sttpBackend)
      .flatMap(_.body)
  }

  override def updatePageInDatabase(
      bearerToken: String,
      updatePageInDatabaseRequest: UpdatePageInDatabaseRequest,
  ): F[NotionPageResponse] = {
    val updatePageInDatabaseUri: Uri =
      uri"${notionClientConfiguration.baseUrl}/v1/pages/"
        .addPath(updatePageInDatabaseRequest.pageId)

    //println(createIssueInDatabaseRequest)

    basicRequest.auth
      .bearer(bearerToken)
      .header("Notion-Version", notionClientConfiguration.notionVersion)
      .patch(updatePageInDatabaseUri)
      .body(updatePageInDatabaseRequest)
      .response(SttpResponseUtils.unwrapResponse[F, NotionPageResponse])
      .readTimeout(notionClientConfiguration.timeout)
      .send(sttpBackend)
      .flatMap(_.body)
  }

  override def getMembersOfNotionTeamSpace(bearerToken: String): F[NotionMembers] = {
    val getMembersOfNotionTeamSpaceUri: Uri =
      uri"${notionClientConfiguration.baseUrl}/v1/users"

    //println(createIssueInDatabaseRequest)

    basicRequest.auth
      .bearer(bearerToken)
      .header("Notion-Version", notionClientConfiguration.notionVersion)
      .get(getMembersOfNotionTeamSpaceUri)
      .response(SttpResponseUtils.unwrapResponse[F, NotionMembers])
      .readTimeout(notionClientConfiguration.timeout)
      .send(sttpBackend)
      .flatMap(_.body)
  }
}

/*
{
  "parent" : {
    "type" : "page_id",
    "page_id" : "f29c170b355d483f9b59e1f9a9fd9a7b"
  },
  "title" : [
    {
      "type" : "text",
      "text" : {
        "content" : "MY MAPPED DB!!!",
        "link" : null
      }
    }
  ],
  "properties" : {
    "name" : {
      "title" : {

      }
    },
    "description" : {
      "rich_text" : {

      }
    },
    "dueDate" : {
      "date" : {

      }
    },
    "tags" : {
      "type" : "multi_select",
      "multi_select" : {
        "options" : [
          {
            "name" : "backend"
          },
          {
            "name" : "devops"
          },
          {
            "name" : "frontend"
          }
        ]
      }
    }
  }
}
 */

//trait OrderServiceImplSpecUtils extends DefaultWirings {
//  val backend: SttpBackend[IO, Any] =
//    AsyncHttpClientCatsBackend.usingClient[IO](new DefaultAsyncHttpClient())
//  val client: HttpOrderClient[IO] = new HttpOrderClient[IO](backend, orderClientConfiguration)
//  val logger: Logger[IO] = Slf4jFactory.create[IO].getLogger
//  val retryUtils: RetryUtilsImpl[IO] = new RetryUtilsImpl[IO](logger, retryConfiguration)
//  val service: RetryingOrderClient[IO] = new RetryingOrderClient[IO](client, retryUtils)
//}
