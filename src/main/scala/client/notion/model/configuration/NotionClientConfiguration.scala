package client.notion.model.configuration

import com.typesafe.config.Config
import net.ceedubs.ficus.Ficus.toFicusConfig
import net.ceedubs.ficus.readers.ValueReader

import scala.concurrent.duration.{FiniteDuration, SECONDS}

case class NotionClientConfiguration(
    baseUrl: String,
    timeout: FiniteDuration,
    myBearerToken: String,
    notionVersion: String,
)

object NotionClientConfiguration {
  def load(config: Config): NotionClientConfiguration =
    config.as[NotionClientConfiguration]("notion.client")

  private implicit val petClientConfigurationReader: ValueReader[NotionClientConfiguration] =
    ValueReader.relative(config =>
      NotionClientConfiguration(
        baseUrl = config.getString("base-url"),
        timeout = FiniteDuration.apply(config.getLong("timeout"), SECONDS),
        myBearerToken = config.getString("headers.my-bearer-token"),
        notionVersion = config.getString("headers.notion-version"),
      ),
    )
}
