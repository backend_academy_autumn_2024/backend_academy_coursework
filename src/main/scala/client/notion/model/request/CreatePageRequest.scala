package client.notion.model.request

import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder

import java.util.UUID

final case class CreatePageRequest(petId: UUID)

object CreatePageRequest {
  implicit val createPageRequestEncoder: Encoder[CreatePageRequest] =
    deriveEncoder[CreatePageRequest]

}
