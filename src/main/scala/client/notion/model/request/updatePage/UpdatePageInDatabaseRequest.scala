package client.notion.model.request.updatePage

import client.notion.model.request.createIssue.NotionPage
import io.circe.{Encoder, Json}

case class UpdatePageInDatabaseRequest(pageId: String, notionPage: NotionPage)

object UpdatePageInDatabaseRequest {
  implicit val createIssueInDatabaseRequestEncoder: Encoder[UpdatePageInDatabaseRequest] =
    new Encoder[UpdatePageInDatabaseRequest] {
      override def apply(a: UpdatePageInDatabaseRequest): Json = Json.obj(
        ("properties", NotionPage.notionPageEncoder.apply(a.notionPage)),
      )
    }
}
