package client.notion.model.request.createDatabase.models.propertiesOfDb.models

import client.notion.model.request.createDatabase.models.propertiesOfDb.PropertyOfDb
import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder

case class NameConfig(title: Unit) extends PropertyOfDb {
  override val propertyName: String = "name"
}

object NameConfig {
  implicit val nameConfigEncoder: Encoder[NameConfig] = deriveEncoder[NameConfig]

}
