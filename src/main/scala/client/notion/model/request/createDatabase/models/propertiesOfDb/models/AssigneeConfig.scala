package client.notion.model.request.createDatabase.models.propertiesOfDb.models

import client.notion.model.request.createDatabase.models.propertiesOfDb.PropertyOfDb
import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder

case class AssigneeConfig(people: Unit) extends PropertyOfDb {
  override val propertyName: String = "assignee"
}

object AssigneeConfig {
  implicit val assigneeConfigEncoder: Encoder[AssigneeConfig] = deriveEncoder[AssigneeConfig]
}
