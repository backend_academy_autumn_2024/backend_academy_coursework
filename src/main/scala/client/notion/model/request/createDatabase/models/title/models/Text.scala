package client.notion.model.request.createDatabase.models.title.models

import io.circe.Encoder
import io.circe.generic.extras.{Configuration, ConfiguredJsonCodec}
import io.circe.generic.semiauto.deriveEncoder

@ConfiguredJsonCodec case class Text(content: String, link: Option[String])

object Text {
  implicit val config: Configuration = Configuration.default

  implicit val textEncoder: Encoder[Text] =
    deriveEncoder[Text]
}
