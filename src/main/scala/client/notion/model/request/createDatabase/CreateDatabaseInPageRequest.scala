package client.notion.model.request.createDatabase

import client.notion.model.request.createDatabase.models.title.Title
import client.notion.model.request.createDatabase.models.PageParent
import client.notion.model.request.createDatabase.models.propertiesOfDb.PropertiesOfDb
import io.circe.generic.semiauto.deriveEncoder
import io.circe.Encoder

final case class CreateDatabaseInPageRequest(
    parent: PageParent,
    title: List[Title],
    properties: PropertiesOfDb,
)
object CreateDatabaseInPageRequest {
  implicit val createDatabaseInPageRequestEncoder: Encoder[CreateDatabaseInPageRequest] =
    deriveEncoder[CreateDatabaseInPageRequest]
}

//class
