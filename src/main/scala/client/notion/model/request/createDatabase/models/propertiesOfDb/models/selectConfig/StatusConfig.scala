package client.notion.model.request.createDatabase.models.propertiesOfDb.models.selectConfig

import client.notion.model.request.createDatabase.models.propertiesOfDb.PropertyOfDb
import io.circe.{Encoder, Json}

case class StatusConfig(Type: String = "select", options: List[SelectOption]) extends PropertyOfDb {

  override val propertyName: String = "status"
}

object StatusConfig {
  implicit val statusConfigEncoder: Encoder[StatusConfig] = new Encoder[StatusConfig] {
    override def apply(a: StatusConfig): Json = Json.obj(
      ("type", Json.fromString(a.Type)),
      (
        "select",
        Json.obj(
          ("options", Json.fromValues(a.options.map(SelectOption.selectOptionEncoder.apply))),
        ),
      ),
    )
  }
}
