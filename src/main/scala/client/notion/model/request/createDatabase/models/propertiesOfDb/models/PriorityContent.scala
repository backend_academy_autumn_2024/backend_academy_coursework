package client.notion.model.request.createDatabase.models.propertiesOfDb.models

import client.notion.model.request.createDatabase.models.propertiesOfDb.models.selectConfig.SelectOption
import io.circe.generic.semiauto.deriveEncoder
import io.circe.{Encoder, Json}

case class PriorityContent(options: List[SelectOption])

object PriorityContent {
  implicit val priorityContentEncoder: Encoder[PriorityContent] = new Encoder[PriorityContent] {
    override def apply(a: PriorityContent): Json = Json.obj(
      ("select", deriveEncoder[PriorityContent].apply(a)),
    )
  }
}
