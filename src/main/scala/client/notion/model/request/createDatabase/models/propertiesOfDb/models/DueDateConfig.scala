package client.notion.model.request.createDatabase.models.propertiesOfDb.models

import client.notion.model.request.createDatabase.models.propertiesOfDb.PropertyOfDb
import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder

case class DueDateConfig(date: Unit) extends PropertyOfDb {

  override val propertyName: String = "dueDate"
}

object DueDateConfig {
  implicit val dueDateConfig: Encoder[DueDateConfig] = deriveEncoder[DueDateConfig]
}
