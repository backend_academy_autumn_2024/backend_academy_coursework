package client.notion.model.request.createDatabase.models.title

import client.notion.model.request.createDatabase.models.title.models.Text
import io.circe.Encoder
import io.circe.generic.extras.{Configuration, ConfiguredJsonCodec, JsonKey}
import io.circe.generic.semiauto.deriveEncoder

@ConfiguredJsonCodec
case class Title(
    @JsonKey("type") titleType: String = "text", //called Type because "type" is keyword in scala
    text: Text,
)

object Title {
  implicit val config: Configuration = Configuration.default
  implicit val titleEncoder: Encoder[Title] =
    deriveEncoder[Title]
}
