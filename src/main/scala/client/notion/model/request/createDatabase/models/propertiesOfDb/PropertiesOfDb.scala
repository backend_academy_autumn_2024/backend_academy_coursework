package client.notion.model.request.createDatabase.models.propertiesOfDb

import client.notion.model.request.createDatabase.models.propertiesOfDb.models.selectConfig.TagsConfig
import client.notion.model.request.createDatabase.models.propertiesOfDb.models.{
  AssigneeConfig,
  DescriptionConfig,
  DueDateConfig,
  NameConfig,
}
import io.circe.{Encoder, Json}
import io.circe.generic.semiauto.deriveEncoder

case class PropertiesOfDb(properties: List[PropertyOfDb])

object PropertiesOfDb {
  implicit val defaultPropertiesOfDb: List[PropertyOfDb] = List(
    NameConfig(()),
    DescriptionConfig(()),
    DueDateConfig(()),
    AssigneeConfig(()),
  )
  implicit val propertiesOfDbEncoder: Encoder[PropertiesOfDb] = new Encoder[PropertiesOfDb] {
    override def apply(props: PropertiesOfDb): Json =
      props.properties.foldLeft(Json.obj()) { (acc, kv) =>
        val key = kv.propertyName
        val value: PropertyOfDb = kv
        acc.deepMerge(PropertyOfDb.propertyOfDbEncoder.apply(value))
      }
  }

}
