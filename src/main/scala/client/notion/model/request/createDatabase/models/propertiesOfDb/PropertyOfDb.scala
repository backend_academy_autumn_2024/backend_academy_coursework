package client.notion.model.request.createDatabase.models.propertiesOfDb

import client.notion.model.request.createDatabase.models.propertiesOfDb.models.selectConfig.{
  StatusConfig,
  TagsConfig,
}
import client.notion.model.request.createDatabase.models.propertiesOfDb.models.{
  AssigneeConfig,
  DescriptionConfig,
  DueDateConfig,
  NameConfig,
}
import io.circe.{Encoder, Json}
import io.circe.generic.semiauto.deriveEncoder

trait PropertyOfDb {
  val propertyName: String
}
//name: NameConfig = NameConfig(()),
//                          description: DescriptionContent = DescriptionContent(()),
//                          //priority: PriorityContent,
//                          dueDate: DueDateConfig = DueDateConfig(()),
//                          tags: TagsConfig

object PropertyOfDb {
  implicit val propertyOfDbEncoder: Encoder[PropertyOfDb] = new Encoder[PropertyOfDb] {
    override def apply(a: PropertyOfDb): Json = Json.obj(
      (
        a.propertyName,
        a match {
          case d: DescriptionConfig => deriveEncoder[DescriptionConfig].apply(d)
          case d: DueDateConfig     => deriveEncoder[DueDateConfig].apply(d)
          case d: NameConfig        => deriveEncoder[NameConfig].apply(d)
          case d: TagsConfig        => TagsConfig.tagsConfigEncoder.apply(d)
          case d: StatusConfig      => StatusConfig.statusConfigEncoder.apply(d)
          case d: AssigneeConfig    => AssigneeConfig.assigneeConfigEncoder.apply(d)
        },
      ),
    )
  }
}
