package client.notion.model.request.createDatabase.models.propertiesOfDb.models.selectConfig

import client.notion.model.request.createDatabase.models.propertiesOfDb.PropertyOfDb
import io.circe.generic.extras.ConfiguredJsonCodec
import io.circe.{Encoder, Json}

case class TagsConfig(Type: String = "multi_select", options: List[SelectOption])
    extends PropertyOfDb {

  override val propertyName: String = "tags"
}

object TagsConfig {
  implicit val tagsConfigEncoder: Encoder[TagsConfig] = new Encoder[TagsConfig] {
    //deriveEncoder[TagsConfig]

    override def apply(a: TagsConfig): Json = Json.obj(
      ("type", Json.fromString(a.Type)), //TODO: maybe change to @JsonKey("type") ?
      (
        "multi_select",
        Json.obj(
          ("options", Json.fromValues(a.options.map(SelectOption.selectOptionEncoder.apply))),
        ),
      ),
    )
  }
}
