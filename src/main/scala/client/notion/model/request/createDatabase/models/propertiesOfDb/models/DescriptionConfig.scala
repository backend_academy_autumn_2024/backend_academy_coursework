package client.notion.model.request.createDatabase.models.propertiesOfDb.models

import client.notion.model.request.createDatabase.models.propertiesOfDb.PropertyOfDb
import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder

case class DescriptionConfig(rich_text: Unit) extends PropertyOfDb {

  override val propertyName: String = "description"
}

object DescriptionConfig {
  implicit val descriptionConfigEncoder: Encoder[DescriptionConfig] =
    deriveEncoder[DescriptionConfig]

}
