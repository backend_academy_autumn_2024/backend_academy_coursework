package client.notion.model.request.createDatabase.models.propertiesOfDb.models.selectConfig

import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder

case class SelectOption(name: String)
//TODO: maybe add color option?

object SelectOption {
  implicit val selectOptionEncoder: Encoder[SelectOption] =
    deriveEncoder[SelectOption]
}
