package client.notion.model.request.createDatabase.models

import io.circe.Encoder
import io.circe.generic.extras.{Configuration, ConfiguredJsonCodec, JsonKey}
import io.circe.generic.semiauto.deriveEncoder

@ConfiguredJsonCodec case class PageParent(
    @JsonKey("type") contentType: String = "page_id",
    page_id: String,
)

object PageParent {
  implicit val config: Configuration = Configuration.default

  implicit val pageParentEncoder: Encoder[PageParent] =
    deriveEncoder[PageParent]
}
