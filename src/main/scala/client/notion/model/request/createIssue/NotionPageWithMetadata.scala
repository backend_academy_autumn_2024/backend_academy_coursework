package client.notion.model.request.createIssue

import cats.Semigroup
import client.commons.utils.encoder.Encode.dropNulls
import client.jb_space.model.response.issue.Issue
import client.notion.model.request.properties.{
  NotionDate,
  NotionMultiSelect,
  NotionRichText,
  NotionSelect,
  NotionText,
}
import io.circe.Decoder.Result
import io.circe.{ACursor, Decoder, DecodingFailure, Encoder, HCursor}
import io.circe.generic.semiauto.deriveEncoder

case class NotionPageWithMetadata(properties: NotionPage)

object NotionPageWithMetadata {
  //implicit val notionPageWithMetadataEncoder: Encoder[NotionPageWithMetadata] = deriveEncoder
  implicit val notionPageWithMetadataDecoder: Decoder[NotionPageWithMetadata] =
    new Decoder[NotionPageWithMetadata] {
      override def apply(c: HCursor): Result[NotionPageWithMetadata] = {
        val some: Either[DecodingFailure, HCursor] = c
          .downField("properties")
          .success
          .toRight(DecodingFailure.apply(DecodingFailure.Reason.MissingField, c))
        val result: Either[DecodingFailure, NotionPageWithMetadata] = {
          for {
            hCursor <- c
              .downField("properties")
              .success
              .toRight(
                DecodingFailure.apply(DecodingFailure.Reason.MissingField, c),
              )
            notionPage <- NotionPage.notionPageDecoder.apply(hCursor)
          } yield (NotionPageWithMetadata(notionPage))
        }
        result
      }
    }
}
