package client.notion.model.request.createIssue

import client.commons.utils.encoder.Encode.dropNulls
import client.jb_space.model.response.issue.Issue
import client.jb_space.model.response.issue.tag.Tag
import client.notion.model.request.properties.{
  NotionAssignee,
  NotionDate,
  NotionMultiSelect,
  NotionRichText,
  NotionSelect,
  NotionText,
  NotionTitle,
}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder, Json}

import java.time.LocalDate

case class NotionPage(
    title: NotionTitle,
    assignee: Option[NotionAssignee],
    description: Option[NotionRichText],
    dueDate: Option[NotionDate],
    tags: NotionMultiSelect,
    status: NotionSelect,
)

object NotionPage {
  implicit val notionPageEncoder: Encoder[NotionPage] = dropNulls(
    deriveEncoder,
  ) //dropNulls for not showing null for None objects

  implicit val notionPageDecoder: Decoder[NotionPage] = deriveDecoder[NotionPage]

  def createFromJSpaceIssue(issue: Issue, assignee: Option[NotionAssignee]): NotionPage =
    NotionPage(
      title = NotionTitle(List(NotionText(issue.title, None))),
      assignee = assignee,
      description =
        issue.description.map(str => NotionRichText(List(NotionText(content = str, link = None)))),
      dueDate = issue.dueDate.map(NotionDate(_)),
      tags = NotionMultiSelect(
        issue.tags
          .map(tag => tag.name),
      ),
      status = NotionSelect(issue.status.name),
    )
}
