package client.notion.model.request.createIssue

import io.circe.{Encoder, Json}

case class CreateIssueInDatabaseRequest(databaseId: String, notionPage: NotionPage)

object CreateIssueInDatabaseRequest {
  implicit val createIssueInDatabaseRequestEncoder: Encoder[CreateIssueInDatabaseRequest] =
    new Encoder[CreateIssueInDatabaseRequest] {
      override def apply(a: CreateIssueInDatabaseRequest): Json = Json.obj(
        ("parent", Json.obj(("database_id", Json.fromString(a.databaseId)))),
        ("properties", NotionPage.notionPageEncoder.apply(a.notionPage)),
      )
    }
}
