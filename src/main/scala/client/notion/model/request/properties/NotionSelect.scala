package client.notion.model.request.properties

import client.notion.model.request.createIssue.NotionPage
import io.circe.Decoder.Result
import io.circe.generic.semiauto.deriveDecoder
import io.circe.{Decoder, DecodingFailure, Encoder, HCursor, Json}

case class NotionSelect(value: String) extends NotionDbProperty {
  override val name: String = "select"
  override val Type: String = "select"
}

//TODO: remove name and Type to companion object
object NotionSelect {
  implicit val notionSelectDecoder: Decoder[NotionSelect] = new Decoder[NotionSelect] {
    override def apply(c: HCursor): Result[NotionSelect] = {
      val result = for {
        selectCursor <- c
          .downField("select")
          .success
          .toRight(DecodingFailure.apply(DecodingFailure.Reason.MissingField, c))
        value <- selectCursor.get[String]("name")
      } yield NotionSelect(value)
      result
    }
  }

  implicit val notionMultiSelect: Encoder[NotionSelect] = new Encoder[NotionSelect] {
    override def apply(a: NotionSelect): Json = Json.obj(
      (
        a.name,
        Json.obj(
          ("name", Json.fromString(a.value)),
        ),
      ),
    )
  }
}
