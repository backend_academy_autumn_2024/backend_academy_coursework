package client.notion.model.request.properties

import io.circe.{Decoder, Encoder, HCursor, Json}

case class NotionTitle(
    content: List[
      NotionText,
    ], //TODO: basically no need to hold it in array (now used because of json and api requirements)
) extends NotionDbProperty {
  override val name: String = "title"
  override val Type: String = "title"
}

object NotionTitle {
  val decodeNotionTexts = Decoder[List[NotionText]].prepare(
    _.downField("title"),
  )

  implicit val notionTitleDecoder: Decoder[NotionTitle] = (c: HCursor) =>
    for {
      result <- decodeNotionTexts.apply(c)
    } yield (NotionTitle(content = result))

  implicit val notionTitleEncoder: Encoder[NotionTitle] = new Encoder[NotionTitle] {
    override def apply(a: NotionTitle): Json = Json.obj(
      (
        a.name,
        Json.fromValues(
          a.content.map(notionText =>
            Json.obj(
              ("type", Json.fromString(notionText.Type)),
              ("text", NotionText.notionTextEncoder.apply(notionText)),
            ),
          ),
        ),
      ),
    )
  }
}
