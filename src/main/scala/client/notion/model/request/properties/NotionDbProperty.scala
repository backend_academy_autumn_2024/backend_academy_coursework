package client.notion.model.request.properties

abstract class NotionDbProperty {
  val name: String
  val Type: String
}
