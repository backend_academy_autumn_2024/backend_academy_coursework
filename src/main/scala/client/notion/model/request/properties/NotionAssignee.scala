package client.notion.model.request.properties

import io.circe.Decoder.Result
import io.circe.{Decoder, DecodingFailure, Encoder, HCursor, Json}

case class NotionAssignee(id: String) extends NotionDbProperty {
  override val name: String = "people"
  override val Type: String = "people"
}

object NotionAssignee {

  implicit val notionAssigneeDecoder: Decoder[NotionAssignee] = new Decoder[NotionAssignee] {
    override def apply(c: HCursor): Result[NotionAssignee] = {
      val result: Option[NotionAssignee] = for {
        array <- c.downField("people").values
        firstAssignee <- array.headOption
        id <- firstAssignee.hcursor.downField("id").as[String].toOption
      } yield (NotionAssignee(id))
      result.toRight(DecodingFailure.apply(DecodingFailure.Reason.MissingField, c))
    }
  }

  implicit val notionAssigneeEncoder: Encoder[NotionAssignee] = new Encoder[NotionAssignee] {
    override def apply(a: NotionAssignee): Json = Json.obj(
      (
        "people",
        Json.arr(
          Json.obj(("object", Json.fromString("user")), ("id", Json.fromString(a.id))),
        ),
      ),
    )
  }

}
