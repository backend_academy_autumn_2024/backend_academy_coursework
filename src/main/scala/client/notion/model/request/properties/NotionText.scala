package client.notion.model.request.properties

import io.circe.Decoder.Result
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, DecodingFailure, Encoder, HCursor, Json}

case class NotionText(content: String, link: Option[String]) extends NotionDbProperty {
  override val name: String = "text"
  override val Type: String = "text"
}

object NotionText {
  implicit val notionTextDecoder: Decoder[NotionText] =
    deriveDecoder[NotionText].prepare(_.downField("text"))
//    new Decoder[NotionText] {
//    override def apply(c: HCursor): Result[NotionText] = {
//      for {
//        text <- c.downField("text").success.toRight(DecodingFailure("Expected 'text' field at 'rich_text'", c.history))
//        content <- text.downField("content").success.toRight(DecodingFailure("Expected 'content' field at 'text'", c.history))
//        fdsa = content.value.toString()
//
//        //link <- text.downField("link")
//      } yield NotionText(content = fdsa, link = None
//      )
//    }
//  }

  implicit val notionTextEncoder: Encoder[NotionText] = deriveEncoder[NotionText]
}
