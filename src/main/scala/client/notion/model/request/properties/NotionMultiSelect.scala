package client.notion.model.request.properties

import client.notion.model.request.createIssue.NotionPage
import io.circe.Decoder.Result
import io.circe.generic.semiauto.deriveDecoder
import io.circe.{Decoder, DecodingFailure, Encoder, HCursor, Json}

case class NotionMultiSelect(list: List[String]) extends NotionDbProperty {

  override val name: String = "multi_select"
  override val Type: String = "multi_select"
}

//TODO: remove name and Type to companion object
object NotionMultiSelect {
  val decodeMultiSelect = Decoder[List[String]].prepare(
    _.downField("multi_select"),
  )
  import cats.syntax.either._

  implicit val notionMultiSelectDecoder: Decoder[NotionMultiSelect] =
    new Decoder[NotionMultiSelect] {
      override def apply(c: HCursor): Result[NotionMultiSelect] = {
        val res = for {
          arrayCursor <- c.downField("multi_select").success
          array <- arrayCursor.values
          values = array.map(_.hcursor.get[String]("name"))
          validValues = values.collect { case Right(value) =>
            value
          }.toList
        } yield NotionMultiSelect(validValues)
        res.toRight(DecodingFailure.apply(DecodingFailure.Reason.MissingField, c))
      }
    }
  // deriveDecoder[NotionMultiSelect]

  implicit val notionMultiSelect: Encoder[NotionMultiSelect] = new Encoder[NotionMultiSelect] {
    override def apply(a: NotionMultiSelect): Json = Json.obj(
      (
        a.name,
        Json.fromValues(
          a.list.map(str =>
            Json.obj(
              ("name", Json.fromString(str)),
            ),
          ),
        ),
      ),
    )
  }
}
