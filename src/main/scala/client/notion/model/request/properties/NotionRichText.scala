package client.notion.model.request.properties

import io.circe.Decoder.Result
import io.circe.generic.semiauto.deriveDecoder
import io.circe.{Decoder, DecodingFailure, Encoder, HCursor, Json}

case class NotionRichText(
    content: List[
      NotionText,
    ], //TODO: basically no need to hold it in array (now used because of json and api requirements)
) extends NotionDbProperty {
  override val name: String = "rich_text"
  override val Type: String = "rich_text"
}

object NotionRichText {

  val decodeNotionTexts = Decoder[List[NotionText]].prepare(
    _.downField("rich_text"),
  )

  implicit val notionRichTextDecoder: Decoder[NotionRichText] = (c: HCursor) =>
    for {
      result <- decodeNotionTexts.apply(c)
    } yield (NotionRichText(content = result))

  implicit val notionRichTextEncoder: Encoder[NotionRichText] = new Encoder[NotionRichText] {
    override def apply(a: NotionRichText): Json = Json.obj(
      (
        a.name,
        Json.fromValues(
          a.content.map(notionText =>
            Json.obj(
              ("type", Json.fromString(notionText.Type)),
              ("text", NotionText.notionTextEncoder.apply(notionText)),
            ),
          ),
        ),
      ),
    )
  }
}
