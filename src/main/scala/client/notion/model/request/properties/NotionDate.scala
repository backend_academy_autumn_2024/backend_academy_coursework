package client.notion.model.request.properties

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder, Json}

import java.time.LocalDate

case class NotionDate(start: LocalDate) extends NotionDbProperty {

  override val name: String = "date"
  override val Type: String = "date"
}

//TODO: remove name and Type to companion object
object NotionDate {

  implicit val notionDateDecoder: Decoder[NotionDate] = deriveDecoder

  implicit val notionDateEncoder: Encoder[NotionDate] = (a: NotionDate) =>
    Json.obj(
      ("date", notionDateBasicEncoder.apply(a)),
    )

  val notionDateBasicEncoder: Encoder[NotionDate] = deriveEncoder[NotionDate]
}
