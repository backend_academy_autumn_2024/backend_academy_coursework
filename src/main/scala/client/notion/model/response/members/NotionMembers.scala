package client.notion.model.response.members

import io.circe.Decoder.Result
import io.circe.{Decoder, DecodingFailure, HCursor}
import org.http4s.DecodeFailure

case class NotionMembers(members: List[NotionMember])

object NotionMembers {

  implicit val membersDecoder: Decoder[NotionMembers] = Decoder.instance { c =>
    val result: List[NotionMember] =
      c.downField("results").values.getOrElse(List()).foldLeft(List.empty[NotionMember]) {
        (members, json) =>
          val decodeResult: Result[NotionMember] =
            json.hcursor.downField("type").as[String].flatMap {
              case "person" => json.as[NotionMember]
              case _        => Left(DecodingFailure("ignored bot member", c.history)) // Ignore bots
            }
          decodeResult match {
            case Right(notionMember) => notionMember :: members
            case Left(_)             => members
          }
      }
    val rightResult: Result[NotionMembers] = Right(NotionMembers(result))
    rightResult
  }

}
