package client.notion.model.response.members

import io.circe.{Decoder, DecodingFailure, HCursor}

case class NotionMember(id: String, name: String, email: String)

object NotionMember {
  implicit val memberDecoder: Decoder[NotionMember] = new Decoder[NotionMember] {
    final def apply(c: HCursor): Decoder.Result[NotionMember] =
      for {
        id <- c.downField("id").as[String]
        name <- c.downField("name").as[String]
        email <- c.downField("person").downField("email").as[String]
      } yield NotionMember(id, name, email)
  }
}
