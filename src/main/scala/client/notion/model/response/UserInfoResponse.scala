package client.notion.model.response

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

import java.util.UUID

case class UserInfoResponse(id: UUID)

object UserInfoResponse {
  implicit val userInfoResponseDecoder: Decoder[UserInfoResponse] = deriveDecoder[UserInfoResponse]
}
