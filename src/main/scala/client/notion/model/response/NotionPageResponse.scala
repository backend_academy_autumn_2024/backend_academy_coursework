package client.notion.model.response

import client.notion.model.request.createIssue.NotionPage
import client.notion.model.request.properties.{
  NotionAssignee,
  NotionDate,
  NotionMultiSelect,
  NotionRichText,
  NotionSelect,
  NotionText,
  NotionTitle,
}
import io.circe.Decoder.Result
import io.circe.{Decoder, DecodingFailure, HCursor}
import io.circe.generic.semiauto.deriveDecoder

import java.time.LocalDate

case class NotionPageResponse(
    id: String,
    title: NotionTitle,
    assignee: Option[NotionAssignee],
    description: Option[NotionRichText],
    dueDate: Option[NotionDate],
    tags: NotionMultiSelect,
    status: NotionSelect,
) {

  def toPage(): NotionPage = NotionPage(title, assignee, description, dueDate, tags, status)
}

object NotionPageResponse {

  val decodeNotionDate = NotionDate.notionDateDecoder.prepare(
    _.downField("date"),
  )

  implicit val notionPageResponseDecoder: Decoder[NotionPageResponse] =
    new Decoder[NotionPageResponse] {
      override def apply(c: HCursor): Result[NotionPageResponse] =
        for {
          id <- c.get[String]("id")
          properties <- c
            .downField("properties")
            .success
            .toRight(DecodingFailure.apply(DecodingFailure.Reason.MissingField, c))

          title <- properties
            .downField("name")
            .success
            .toRight(DecodingFailure.apply(DecodingFailure.Reason.MissingField, c))
          validTitle <- NotionTitle.notionTitleDecoder.apply(title)

          assignee <- properties
            .downField("assignee")
            .success
            .toRight(DecodingFailure.apply(DecodingFailure.Reason.MissingField, c))

          description <- properties
            .downField("description")
            .success
            .toRight(DecodingFailure.apply(DecodingFailure.Reason.MissingField, c))
          dueDate <- properties
            .downField("dueDate")
            .success
            .toRight(DecodingFailure.apply(DecodingFailure.Reason.MissingField, c))

          tags <- properties
            .downField("tags")
            .success
            .toRight(DecodingFailure.apply(DecodingFailure.Reason.MissingField, c))
          validTags <- NotionMultiSelect.notionMultiSelectDecoder.apply(tags)

          status <- properties
            .downField("status")
            .success
            .toRight(DecodingFailure.apply(DecodingFailure.Reason.MissingField, c))
          validStatus <- NotionSelect.notionSelectDecoder(status)

          result = NotionPageResponse(
            id = id,
            title = validTitle,
            assignee = NotionAssignee.notionAssigneeDecoder.apply(assignee).toOption,
            description = NotionRichText.notionRichTextDecoder.apply(description).toOption,
            dueDate = decodeNotionDate.apply(dueDate).toOption,
            tags = validTags,
            status = validStatus,
          )
        } yield result
    }
}
