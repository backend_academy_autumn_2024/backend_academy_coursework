package client.notion.model.response

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

import java.time.Instant
import java.util.UUID

case class PageResponse(id: String)

object PageResponse {
  implicit val orderResponseDecoder: Decoder[PageResponse] = deriveDecoder[PageResponse]
}
