package client.notion.wirings

import cats.effect.IO
import client.notion.model.configuration.NotionClientConfiguration
import com.typesafe.config.{Config, ConfigFactory}
import org.asynchttpclient.util.HttpConstants.Methods
import sttp.client3.asynchttpclient.cats.AsyncHttpClientCatsBackend
import sttp.client3.{Response, SttpBackend}
import sttp.model.StatusCode

trait MyDefaultWirings extends TestUtils {
  val config: Config = ConfigFactory.load()
  val notionClientConfiguration: NotionClientConfiguration = NotionClientConfiguration.load(config)
  //val retryConfiguration: RetryConfiguration = RetryConfiguration.load(config)
  val sttpBackend: SttpBackend[IO, Any] = AsyncHttpClientCatsBackend
    .stub[IO]
    .whenRequestMatchesPartial {
//      case r if r.uri.toString().contains("api/v1/order") && r.method.toString() == Methods.POST =>
//        Response.ok(
//          IO.pure(
//            parseAsJsonUnsafe(orderResponseString)
//              .as[OrderResponse]
//              .fold(_.toString(), response => response),
//          ),
//        )
//      case r if r.uri.toString().contains(s"api/v1/order") && r.method.toString() == Methods.GET =>
//        Response.ok(
//          IO.pure(
//            parseAsJsonUnsafe(orderResponseString)
//              .as[Option[OrderResponse]]
//              .fold(_.toString(), response => response),
//          ),
//        )
      case _ => Response("Not found", StatusCode.BadGateway)
    }
}
