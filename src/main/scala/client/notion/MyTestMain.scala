package client.notion

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import client.commons.SttpResponseUtils
import client.notion.model.configuration.NotionClientConfiguration
import client.notion.model.request.createDatabase.{CreateDatabaseInPageRequest, models}
import client.notion.model.request.CreatePageRequest
import client.notion.model.request.createDatabase.models.propertiesOfDb.PropertiesOfDb
import client.notion.model.request.createDatabase.models.title.Title
import client.notion.model.request.createDatabase.models.title.models.Text
import client.notion.model.request.createDatabase.models.PageParent
import client.notion.model.request.createDatabase.models.propertiesOfDb.models.selectConfig.{
  SelectOption,
  TagsConfig,
}
import client.notion.model.request.createDatabase.models.propertiesOfDb.models.{
  DescriptionConfig,
  DueDateConfig,
  PriorityContent,
}
import client.notion.model.response.{PageResponse, UserInfoResponse}
import com.typesafe.config.{Config, ConfigFactory}
import io.circe.Json
import org.asynchttpclient.DefaultAsyncHttpClient
import sttp.client3
import sttp.client3.asynchttpclient.cats.AsyncHttpClientCatsBackend
import sttp.client3.{HttpClientSyncBackend, Response, SttpBackend, UriContext, basicRequest}
import sttp.model.Uri

import java.util.UUID

object MyTestMain extends App {

  val someList: List[Int] = List(1,2,3,4,5,6,7,8,9,10)
  val someList1: IterableOnce[Int] = List()

  import scala.collection.IterableOnce
  import scala.collection.mutable.ArrayBuffer


  val descriptionAsProperty: DescriptionConfig = DescriptionConfig(())

  val someParent: PageParent = PageParent(page_id = "32321213")

  val someText: Text = Text("something", None)
  val someTitle: Title = Title(text = someText)
  val somePriorityContent: PriorityContent = PriorityContent(
    List(
      SelectOption("IMPORTANT"),
      SelectOption("IMPORTANT1"),
    ),
  )
  val someTags: TagsConfig = TagsConfig(options =
    List(
      SelectOption("backend"),
      SelectOption("frontend"),
    ),
  )
  val propertiesOfDb: PropertiesOfDb = PropertiesOfDb(
    //priority = somePriorityContent,
    List(someTags),
  )
  val createDatabaseInPageRequest: CreateDatabaseInPageRequest =
    CreateDatabaseInPageRequest(someParent, List(someTitle), propertiesOfDb)
  val json2: Json = CreateDatabaseInPageRequest.createDatabaseInPageRequestEncoder.apply(
    createDatabaseInPageRequest,
  )

  println(json2.toString())
  val config: Config = ConfigFactory.load()
  val notionClientConfiguration: NotionClientConfiguration = NotionClientConfiguration.load(config)
//  val backend: SttpBackend[IO, Any] =
//    AsyncHttpClientCatsBackend.usingClient[IO](new DefaultAsyncHttpClient())
//
//  val notionClient: HttpNotionClient[IO] = new HttpNotionClient[IO](backend, notionClientConfiguration)
//
//  val createPageRequest: CreatePageRequest = CreatePageRequest(UUID.randomUUID())

  val createOrderUrl: Uri =
    uri"${notionClientConfiguration.baseUrl}/v1/users/me"
  val backend = HttpClientSyncBackend()

  val request = basicRequest
    .get(createOrderUrl)
    .auth
    .bearer(notionClientConfiguration.myBearerToken)
    .header("Notion-Version", notionClientConfiguration.notionVersion)
    .response(SttpResponseUtils.unwrapResponse[IO, UserInfoResponse])
  import cats.implicits._

  val response: IO[UserInfoResponse] = request.send(backend).flatMap(_.body)
  val result: UserInfoResponse = response.unsafeRunSync()

  //println(response.header("Content-Length"))
  println(result)

}
