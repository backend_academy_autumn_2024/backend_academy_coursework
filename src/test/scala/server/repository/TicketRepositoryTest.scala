package server.repository

import cats.effect.{IO, Resource}
import cats.effect.testing.scalatest.{AsyncIOSpec, CatsResourceIO}
import cats.implicits.catsSyntaxOptionId
import doobie.Transactor
import org.scalatest.flatspec.FixtureAsyncFlatSpec
import org.scalatest.matchers.should.Matchers
import server.domain.board.BoardConnection
import server.domain.user.User
import server.repository.postgresql.{BoardRepositoryPostgresql, TicketRepositoryPostgresql, UserRepositoryPostgresql}

import java.util.UUID

class TicketRepositoryTest
    extends FixtureAsyncFlatSpec
    with AsyncIOSpec
    with CatsResourceIO[Transactor[IO]]
    with Matchers {

  override val resource: Resource[IO, Transactor[IO]] = CommonResource.resource

  "TicketRepository" should "return None if db is empty" in { implicit t =>
    val repo = new TicketRepositoryPostgresql[IO]
    val ticketId = UUID.randomUUID()
    for {
      _ <- repo.get(ticketId).asserting(_ shouldBe None)
      _ <- repo.delete(ticketId).asserting(_ shouldBe None)
    } yield ()
  }

//  it should "insert and delete ticket in db" in { implicit t =>
//    val repo = new TicketRepositoryPostgresql[IO]
//
//  }
}
