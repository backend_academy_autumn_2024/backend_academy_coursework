package server.repository

import cats.effect.{IO, Resource}
import cats.effect.testing.scalatest.{AsyncIOSpec, CatsResourceIO}
import cats.implicits.catsSyntaxOptionId
import doobie.Transactor
import org.scalatest.flatspec.FixtureAsyncFlatSpec
import org.scalatest.matchers.should.Matchers
import server.domain.board.BoardConnection
import server.domain.user.User
import server.repository.postgresql.{BoardRepositoryPostgresql, UserRepositoryPostgresql}

import java.util.UUID

class BoardRepositoryTest
    extends FixtureAsyncFlatSpec
    with AsyncIOSpec
    with CatsResourceIO[Transactor[IO]]
    with Matchers
    {
  override val resource: Resource[IO, Transactor[IO]] = CommonResource.resource
      val existedUserId: UUID = UUID.randomUUID()


  "before all" should "setup existing user in db" in { implicit t =>
    val userRepo = new UserRepositoryPostgresql[IO]

    val user = User(
      id = existedUserId,
      notionBearerToken = "notionBearerToken",
      jspaceUsername = "jspaceUsername",
      jetbrainsPermanentToken = "jetbrainsPermanentToken",
    )
    for {
      _ <- userRepo.create(user)
    } yield ()
  }

  "BoardRepository" should "return None if db is empty" in { implicit t =>
    val repo = new BoardRepositoryPostgresql[IO]
    val uuid = UUID.randomUUID()

    for {
      _ <- repo.list.asserting(_ shouldBe List())
      _ <- repo.get(uuid).asserting(_ shouldBe None)
      _ <- repo.delete(uuid).asserting(_ shouldBe None)
    } yield ()

  }

  it should "insert and delete board for existed user in db" in { implicit t =>
    val repo = new BoardRepositoryPostgresql[IO]
    val boardUUID = UUID.randomUUID()

    val board = BoardConnection(
      id = boardUUID,
      notionDatabaseId = "notionDatabaseId",
      jetbrainsBoardId = "jetbrainsBoardId",
      userId = existedUserId,
    )

    for {
      _ <- repo.create(board)

      _ <- repo.list.asserting(_.size shouldBe 1)
      _ <- repo.get(boardUUID).asserting(_ shouldBe board.some)
      _ <- repo.delete(boardUUID).asserting(_ shouldBe board.some)

      _ <- repo.list.asserting(_.size shouldBe 0)
      _ <- repo.get(boardUUID).asserting(_ shouldBe None)
      _ <- repo.delete(boardUUID).asserting(_ shouldBe None)
    } yield ()
  }
}
