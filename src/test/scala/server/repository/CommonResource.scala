package server.repository

import cats.effect.{IO, Resource}
import com.dimafeng.testcontainers.PostgreSQLContainer
import doobie.Transactor
import org.testcontainers.containers.wait.strategy.{LogMessageWaitStrategy, Wait, WaitAllStrategy, WaitStrategy}
import org.testcontainers.utility.DockerImageName
import server.config.PostgresConfig
import server.database.FlywayMigration
import server.database.transactor.makeTransactor

object CommonResource {

  val resource: Resource[IO, Transactor[IO]] = for {
    c <- containerResource
    conf = PostgresConfig(
      c.jdbcUrl,
      user = c.username,
      password = c.password,
      poolSize = 2,
    )
    _ <- Resource.eval(FlywayMigration.migrate[IO](conf))
    tx <- makeTransactor[IO](conf)
  } yield tx

  private val defaultWaitStrategy: WaitStrategy = new WaitAllStrategy()
    .withStrategy(Wait.forListeningPort())
    .withStrategy(
      new LogMessageWaitStrategy()
        .withRegEx(".*database system is ready to accept connections.*\\s")
        .withTimes(2),
    )

  private def containerResource: Resource[IO, PostgreSQLContainer] =
    Resource.make(
      IO {
        val c = PostgreSQLContainer
          .Def(
            dockerImageName = DockerImageName.parse("postgres:14.7"),
          )
          .start()
        c.container.waitingFor(defaultWaitStrategy)
        c
      },
    )(c => IO(c.stop()))
}
