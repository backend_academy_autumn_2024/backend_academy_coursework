package server.repository

import cats.effect.{IO, Resource}
import cats.effect.testing.scalatest.{AsyncIOSpec, CatsResourceIO}
import com.dimafeng.testcontainers.PostgreSQLContainer
import doobie.Transactor
import org.scalatest.flatspec.FixtureAsyncFlatSpec
import org.scalatest.funsuite.AnyFunSuiteLike
import org.scalatest.matchers.should.Matchers
import org.testcontainers.containers.wait.strategy.{
  LogMessageWaitStrategy,
  Wait,
  WaitAllStrategy,
  WaitStrategy,
}
import org.testcontainers.utility.DockerImageName
import server.config.PostgresConfig
import server.database.FlywayMigration
import server.database.transactor.makeTransactor
import server.domain.user.User
import server.repository.postgresql.UserRepositoryPostgresql
import cats.implicits.catsSyntaxOptionId

import java.util.UUID

class UserRepositoryTest
    extends FixtureAsyncFlatSpec
    with AsyncIOSpec
    with CatsResourceIO[Transactor[IO]]
    with Matchers {

  override val resource: Resource[IO, Transactor[IO]] = CommonResource.resource


  "UserRepository" should "return None if db is empty" in { implicit t =>
    val repo = new UserRepositoryPostgresql[IO]
    val uuid = UUID.randomUUID()

    for {
      _ <- repo.list.asserting(_.isEmpty shouldBe true)
      _ <- repo.get(uuid).asserting(_ shouldBe None)
      _ <- repo.delete(uuid).asserting(_ shouldBe None)
    } yield ()
  }

  it should "insert and delete user in db" in { implicit t =>
    val repo = new UserRepositoryPostgresql[IO]
    val uuid = UUID.randomUUID()

    val user = User(uuid, "fdsa", "uiop", "rewq")
    //_ <- repo.create(someUser)

    for {
      _ <- repo.create(user)

      _ <- repo.list.asserting(_.size shouldBe 1)
      _ <- repo.get(uuid).asserting(_ shouldBe user.some)
      _ <- repo.delete(uuid).asserting(_ shouldBe user.some)

      _ <- repo.list.asserting(_.size shouldBe 0)
      _ <- repo.get(uuid).asserting(_ shouldBe None)
      _ <- repo.delete(uuid).asserting(_ shouldBe None)

    } yield ()
  }
}
