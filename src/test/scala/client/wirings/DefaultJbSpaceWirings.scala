package client.wirings

import cats.effect.IO
import client.commons.ClientTestUtils
import client.jb_space.model.configuration.JBSpaceClientConfiguration
import client.jb_space.model.response.issue.IssueResponse
import com.typesafe.config.{Config, ConfigFactory}
import org.asynchttpclient.util.HttpConstants.Methods
import sttp.client3.{Response, SttpBackend}
import sttp.client3.asynchttpclient.cats.AsyncHttpClientCatsBackend
import sttp.model.StatusCode

trait DefaultJbSpaceWirings extends ClientTestUtils{
  val config: Config = ConfigFactory.load()
  val jBSpaceClientConfiguration: JBSpaceClientConfiguration = JBSpaceClientConfiguration.load(config)
  val sttpBackend: SttpBackend[IO, Any] = AsyncHttpClientCatsBackend
    .stub[IO]
    .whenRequestMatchesPartial {
      case r if r.uri.toString().contains("api/v1/order") && r.method.toString() == Methods.POST =>
        Response.ok(
          IO.pure(
            parseAsJsonUnsafe("SOME JSON")
              .as[IssueResponse]
              .fold(_.toString(), response => response),
          ),
        )
      case _ => Response("Not found", StatusCode.BadGateway)
    }

}
