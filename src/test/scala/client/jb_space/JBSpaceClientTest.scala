package client.jb_space

import cats.effect.IO
import cats.effect.testing.scalatest.AsyncIOSpec
import client.jb_space.model.response.issue.IssueResponse
import client.wirings.DefaultJbSpaceWirings
import com.dimafeng.testcontainers.{ContainerDef, DockerComposeContainer, ExposedService}
import com.dimafeng.testcontainers.scalatest.TestContainerForAll
import com.softwaremill.macwire.wire
import org.scalatest.freespec.AsyncFreeSpec
import org.scalatest.funsuite.AnyFunSuiteLike
import org.scalatest.matchers.should.Matchers
import org.testcontainers.containers.wait.strategy.Wait

import java.io.File

class JBSpaceClientTest
    extends AsyncFreeSpec
    with AsyncIOSpec
    with Matchers
    with JBSpaceClientImplSpecUtils {

  "findOrder" - {
    "find order by id" in {
      client
        .getAllIssuesOnBoard("sometoken","someuser", "someboard")
        .asserting(_ shouldBe IssueResponse(List()))
    }
  }
}

trait JBSpaceClientImplSpecUtils extends DefaultJbSpaceWirings {
  val client: JBSpaceClient[IO] = wire[HttpJBSpaceClient[IO]]
}
