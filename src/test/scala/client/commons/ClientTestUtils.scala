package client.commons

import io.circe.Json
import io.circe.parser.parse

trait ClientTestUtils {
  def parseAsJsonUnsafe(str: String): Json = parse(str).fold(
    _ => throw new RuntimeException("Json's parsing is failed"),
    identity,
  )
}
